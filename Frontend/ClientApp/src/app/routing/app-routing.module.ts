import { LoginComponent } from './../components/login/login.component';
import { RegisterComponent } from './../components/register/register.component';
import { StudentsComponent } from '../components/students/students.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentInformationComponent } from '../components/student-information/student-information.component';
import { AuthGuard } from '../helpers/auth-guard';
import { AnonymousGuard } from '../helpers/anonymous-guard';
import { TeacherAccountComponent } from '../components/teacher-account/teacher-account.component';
import { TestComponent } from '../components/test/test.component';
import { ResultComponent } from '../components/result/result.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent, canActivate: [AnonymousGuard] },
  { path: 'login', component: LoginComponent, canActivate: [AnonymousGuard] },
  {
    path: 'students', component: StudentsComponent, canActivate: [AuthGuard],
    data: {
      role: ['Admin']
    }
  },
  {
    path: 'studentInformation', component: StudentInformationComponent, canActivate: [AuthGuard],
    data: {
      role: ['Student']
    }
  },
  {
    path: 'teacherAccount', component: TeacherAccountComponent, canActivate: [AuthGuard],
    data: {
      role: ['Teacher']
    }
  },
  {
    path: 'test', component: TestComponent, canActivate: [AuthGuard],
    data: {
      role: ['Student']
    }
  },
  {
    path: 'result', component: ResultComponent, canActivate: [AuthGuard],
    data: {
      role: ['Student']
    }
  }
  ,
  {
    path: '**', component: NotFoundComponent
  }
];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
