﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// Object of TestingDbContext to access the database context.
        /// </summary>
        private readonly TestingDbContext _dataBase;

        /// <summary>
        /// Object instantiation.
        /// </summary>
        /// <param name="context">Object of TestingDbContext to access the database context.</param>
        public UserRepository(TestingDbContext context)
        {
            _dataBase = context;
        }

        /// <summary>
        /// Gets all users in database.
        /// </summary>
        /// <returns>All users entities in database</returns>
        public IQueryable<User> FindAll()
        {
            return _dataBase.Users;
        }

        /// <summary>
        /// Gets user from database by id async.
        /// </summary>
        /// <param name="id">id of user</param>
        /// <returns>User entity from database</returns>
        public async Task<User> GetByIdAsync(int id)
        {
            return await _dataBase.Users.FindAsync(id);
        }

        /// <summary>
        /// Adds user entity to database.
        /// </summary>
        /// <param name="entity">User entity</param>
        public async Task AddAsync(User entity)
        {
            await _dataBase.Users.AddAsync(entity);
            await _dataBase.SaveChangesAsync();
        }

        /// <summary>
        /// Updates user entity in database.
        /// </summary>
        /// <param name="entity">User entity</param>
        public void Update(User entity)
        {
            _dataBase.Entry(entity).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes user entity.
        /// </summary>
        /// <param name="entity">User entity</param>
        public void Delete(User entity)
        {
            _dataBase.Users.Remove(entity);
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes user entity in database by id async.
        /// </summary>
        /// <param name="id">id of user entity</param>
        public async Task DeleteByIdAsync(int id)
        {
            User user = await _dataBase.Users.FindAsync(id);

            if (user != null)
            {
                _dataBase.Users.Remove(user);
                await _dataBase.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Gets user entity from database by expression async.
        /// </summary>
        /// <param name="expression">Expression by which to search for the entity of the user.</param>
        /// <returns>User Entity</returns>
        public async Task<User> GetByAsync(Expression<Func<User, bool>> expression)
        {
            return await _dataBase.Users.FirstOrDefaultAsync(expression);
        }
    }
}