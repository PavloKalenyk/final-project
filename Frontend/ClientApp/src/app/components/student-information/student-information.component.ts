import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../interfaces/User';
import { Student } from '../../interfaces/Student';
import { StudentService } from 'src/app/services/student.service';
import { AuthService } from 'src/app/services/auth.service';
import { Group } from 'src/app/interfaces/Group';
import { GroupService } from 'src/app/services/group.service';
import { TestService } from 'src/app/services/test.service';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-student-information',
  templateUrl: './student-information.component.html',
  styleUrls: ['./student-information.component.css']
})
export class StudentInformationComponent implements OnInit {

  constructor(private studentService: StudentService,
    private auth: AuthService,
    private groupService: GroupService,
    public testService: TestService,
    private router: Router) { }

  students: User[] = [];
  teachers: User[] = [];
  showResultsParam: boolean = false;
  student: Student;
  group: Group;
  studentResults: any[];

  tests: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;

  testName: string;

  testId: number;

  currentUser = this.auth.getCurrentUserValue();

  ngOnInit(): void {
    console.log("qnProgress");
    console.log();

    console.log("current is is");
    console.log(this.auth.getCurrentUserValue());
    console.log("current user id");
    console.log(this.currentUser);
    if (parseInt(localStorage.getItem('currTest')) == 1) {
      this.router.navigate(['/test']);
    }
    else {
      this.studentService.getStudent(this.currentUser.id).subscribe((student) => {
        console.log("student");
        console.log(student);
        console.log("student user");
        console.log(student.user);

        this.student = student;
        this.testService.studentId = this.student.id;
        localStorage.setItem('studentId', this.student.id.toString());
        this.testService.getStudentResults().subscribe((data) => {
          this.studentResults = data;
          console.log("Student Result is");
          console.log(this.studentResults);
        });

        this.groupService.getGroup(this.student.groupId).subscribe((group) => {
          console.log("group");
          this.group = group;
          console.log(this.group);
          this.testService.getTests(this.group.id).subscribe((tests) => {
            console.log("Tests:");

            // let array = tests.map(item => {
            //   return {
            //     $key: item.key,
            //     ...item.payload.val
            //   };
            // });

            for (let index = 0; index < tests.length; index++) {
              if (tests[index].questionsAnswers.length > 9)
                this.tests.push(tests[index]);
            }

            //let array = [];

            this.listData = new MatTableDataSource(this.tests);
            this.listData.sort = this.sort;
            this.listData.paginator = this.paginator;
            this.listData.filterPredicate = (data, filter) => {
              return this.displayedColumns.some(ele => {
                return ele !== 'actions' && data[ele].toLowerCase().indexOf(filter) !== -1;
              });
            };

            console.log(this.tests);
            // console.log("array");
            // console.log(array);
            console.log("listData");
            console.log(this.listData);
          });
        });
      });

      this.studentService.getStudents().subscribe((data) => {
        this.students = data;
      });

      this.studentService.getTeachers().subscribe((data) => {
        console.log(data);
        this.teachers = data;
      });

    }
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  search() {
    if (this.testName !== '') {
      this.tests = this.tests.filter(res => {
        return res.name.toLocaleLowerCase().match(this.testName.toLocaleLowerCase());
      });
    } else if (this.testName === '') {
      this.ngOnInit();
    }
  }

  addStudent(student: User): void {
    this.studentService.addUser(student).subscribe((data: any) => {
      console.log(data);
      this.studentService.getStudents().subscribe((data) => {
        this.students = data;
        console.log(data);
      });
    },
      (error) => { console.log(error); }
    );
  }

  deleteStudent(studentId: number): void {
    this.studentService.deleteStudent(studentId)
      .subscribe((data) => {
        console.log(data);
        this.studentService.getStudents().subscribe((students) => {
          this.students = students;
          console.log(data);
        });
        this.studentService.getTeachers().subscribe((teachers) => {
          this.teachers = teachers;
        });
      });
  }

  apply(user: User): void {
    user["isActivate"] = true;
    this.studentService.updateUser(user).subscribe((data) => {
      this.studentService.getStudents().subscribe((students) => {
        this.students = students;
      });
      this.studentService.getTeachers().subscribe((teachers) => {
        this.teachers = teachers;
      });
    });
  }

  goToTest(id: number, testName: string) {
    if (!isNaN(id)) {
      localStorage.setItem('testId', id.toString());
    }
    this.testService.testName = testName;
    console.log("TEEEEST");
    console.log(this.testService.testName);
    this.testService.testId = parseInt(localStorage.getItem('testId'));
    this.router.navigate(['/test']);
  }

  showResults() {
    this.showResultsParam = true;
  }

  goToMain() {
    this.showResultsParam = false;
  }
}
