import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TestService } from 'src/app/services/test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor(private router: Router, public testService: TestService) { }


  ngOnInit(): void {
    this.testService.testId = parseInt(localStorage.getItem('testId'));
    this.testService.studentId = parseInt(localStorage.getItem('studentId'));
    console.log("Test is work!");
    console.log(this.testService.testId);
    console.log("testServiceStudentIdTEST");
    console.log(this.testService.studentId);

    if (parseInt(localStorage.getItem('seconds')) > 0) {
      console.log("work1");
      this.testService.seconds = parseInt(localStorage.getItem('seconds'));
      this.testService.qnProgress = parseInt(localStorage.getItem('qnProgress'));
      this.testService.qns = JSON.parse(localStorage.getItem('qns'));
      if (this.testService.qnProgress == 10)
        this.router.navigate(['/result']);
      else {
        this.startTimer();
      }
    }
    else {
      console.log("work2");
      this.testService.seconds = 0;
      this.testService.qnProgress = 0;
      localStorage.setItem('currTest', "1");
      console.log("set item is 1");
      console.log(parseInt(localStorage.getItem('currTest')));
      this.testService.getQuestions(this.testService.testId).subscribe(
        (data: any) => {
          this.testService.qns = data;
          this.startTimer();
        }
      );
    }
  }

  startTimer() {
    this.testService.timer = setInterval(() => {
      this.testService.seconds++;
      localStorage.setItem('seconds', this.testService.seconds.toString());
    }, 1000);
  }

  Answer(qID, choice) {
    this.testService.qns[this.testService.qnProgress].answer = choice;
    localStorage.setItem('qns', JSON.stringify(this.testService.qns));
    this.testService.qnProgress++;
    localStorage.setItem('qnProgress', this.testService.qnProgress.toString());
    if (this.testService.qnProgress == 10) {
      clearInterval(this.testService.timer);
      this.router.navigate(['/result']);
    }
  }
}
