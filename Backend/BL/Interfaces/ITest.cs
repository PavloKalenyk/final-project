﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BL.Models;

namespace BL.Interfaces
{
    public interface ITest : ICrud<QuestionAnswerModel>
    {
        public IEnumerable<TestModel> GetAllNotPassedTests(int groupId, int studentId);
        public Task AddResult(ResultModel model);
        public Task<TestModel> AddTestName(TestModel model, string groupName);
        public Task<QuestionAnswerModel> AddTest(QuestionAnswerModel model, string testName);

        public IEnumerable<QuestionAnswerModel> GetTeacherCurrentTests(string testName);

        public IEnumerable<ResultModel> GetAllResultWithDetails(int studentId);

        public IEnumerable<TestModel> GetAllTestsForTeacher(int teacherId);

        public IEnumerable<TestModel> GetTeacherTestsWithDetails(int teacherId);

        public IEnumerable<ResultModel> GetResultsForTeacher(int testId);

        public Task UpdateTestAsync(TestModel testModel);

        public Task DeleteByIdQuestionAnswerAsync(int modelId);

        public IEnumerable<object> GetQuestionsByTestId(int id);

        public int[] GetAnswers(int[] qIDs, int id);
    }
}