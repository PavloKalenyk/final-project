﻿using System;
using AutoMapper;
using BL.Models;
using DAL.Entities;

namespace BL
{
    public class AutomapperProfile : Profile    
    {
        public AutomapperProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<UserModel, User>();

            CreateMap<Group, GroupModel>();
            CreateMap<GroupModel, Group>();

            CreateMap<Student, StudentModel>();
            CreateMap<StudentModel, Student>();

            CreateMap<Teacher, TeacherModel>();
            CreateMap<TeacherModel, Teacher>();

            CreateMap<QuestionAnswer, QuestionAnswerModel>();
            CreateMap<QuestionAnswerModel, QuestionAnswer>();

            CreateMap<Test, TestModel>();

                //.ForMember(tM => tM.QuestionsAnswers, opt => opt.MapFrom(t => t.QuestionsAnswers))
                //.ReverseMap()
                //.AfterMap((t, tm) =>
                //{
                //    foreach (var qa in tm.QuestionsAnswers)
                //    {
                //        qa.TestId = t.Id;
                //    }
                //}); 

            CreateMap<TestModel, Test>();

            CreateMap<Result, ResultModel>();
            CreateMap<ResultModel, Result>();


            //CreateMap<Result, ResultModel>()
            //    .ForPath(x => x.TestName, y => y.MapFrom(x => x.Test.Name));

            //CreateMap<ResultModel, Result>()
            //    .ForPath(x => x.Test.Name, y => y.MapFrom(x => x.TestName))
            //    .ForPath(x => x.Test.Id, y => y.MapFrom(x => x.TestId)
            //    ;
        }
    }
}
