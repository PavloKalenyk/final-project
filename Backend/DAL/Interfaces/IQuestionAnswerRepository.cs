﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IQuestionAnswerRepository : IRepository<QuestionAnswer>
    {
        public Task<QuestionAnswer> GetByAsync(Expression<Func<QuestionAnswer, bool>> expression);
    }
}