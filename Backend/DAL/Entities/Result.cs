﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Result
    {
        [Key]
        public int Id { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int TestResult { get; set; }
        public int Time { get; set; }
        public int? TestId { get; set; }
        public Test Test { get; set; }
        public bool IsPassedTest { get; set; }
    }
}