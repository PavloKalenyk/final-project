﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class QuestionAnswerRepository : IQuestionAnswerRepository
    {
        /// <summary>
        /// Object of TestingDbContext to access the database context.
        /// </summary>
        private readonly TestingDbContext _dataBase;

        /// <summary>
        /// Object instantiation.
        /// </summary>
        /// <param name="context">Object of TestingDbContext to access the database context.</param>
        public QuestionAnswerRepository(TestingDbContext context)
        {
            _dataBase = context;
        }

        /// <summary>
        /// Gets all QuestionAnswer in database.
        /// </summary>
        /// <returns>All QuestionsAnswers entities in database</returns>
        public IQueryable<QuestionAnswer> FindAll()
        {
            var dqa = _dataBase.QuestionsAnswers;
            return dqa;
        }

        /// <summary>
        /// Gets QuestionAnswer from database by id async.
        /// </summary>
        /// <param name="id">id of QuestionAnswer</param>
        /// <returns>QuestionAnswer entity from database</returns>
        public async Task<QuestionAnswer> GetByIdAsync(int id)
        {
            return await _dataBase.QuestionsAnswers.FindAsync(id);
        }

        /// <summary>
        /// Adds QuestionAnswer entity to database.
        /// </summary>
        /// <param name="entity">QuestionAnswer entity</param>
        public async Task AddAsync(QuestionAnswer entity)
        {
            await _dataBase.QuestionsAnswers.AddAsync(entity);
            await _dataBase.SaveChangesAsync();
        }

        /// <summary>
        /// Updates QuestionAnswer entity in database.
        /// </summary>
        /// <param name="entity">QuestionAnswer entity</param>
        public void Update(QuestionAnswer entity)
        {
            _dataBase.Entry(entity).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes QuestionAnswer entity.
        /// </summary>
        /// <param name="entity">QuestionAnswer entity</param>
        public void Delete(QuestionAnswer entity)
        {
            _dataBase.QuestionsAnswers.Remove(entity);
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes questionAnswer entity in database by id async.
        /// </summary>
        /// <param name="id">id of questionAnswer entity</param>
        public async Task DeleteByIdAsync(int id)
        {
            QuestionAnswer questionAnswer = await _dataBase.QuestionsAnswers.FindAsync(id);

            if (questionAnswer != null)
            {
                _dataBase.QuestionsAnswers.Remove(questionAnswer);
                await _dataBase.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Gets user entity from database by expression async.
        /// </summary>
        /// <param name="expression">Expression by which to search for the entity of the user.</param>
        /// <returns>User Entity</returns>
        public async Task<QuestionAnswer> GetByAsync(Expression<Func<QuestionAnswer, bool>> expression)
        {
            return await _dataBase.QuestionsAnswers.FirstOrDefaultAsync(expression);
        }
    }
}