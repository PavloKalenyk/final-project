﻿namespace BL.Models
{
    public class UserRegisterModel
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GroupName { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public int UserId { get; set; }
    }
}