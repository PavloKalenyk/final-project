﻿using System;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public sealed class TestingDbContext : DbContext
    {
        /// <summary>
        /// Constructor that creates a database if it not created.
        /// </summary>
        /// <param name="options"></param>
        public TestingDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// DbSet of users
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// DbSet of students
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// DbSet of teachers
        /// </summary>
        public DbSet<Teacher> Teachers { get; set; }

        /// <summary>
        /// DbSet of admins
        /// </summary>
        public DbSet<Admin> Admins { get; set; }

        /// <summary>
        /// DbSet of groups
        /// </summary>
        public DbSet<Group> Groups { get; set; }

        /// <summary>
        /// DbSet of tests
        /// </summary>
        public DbSet<Test> Tests { get; set; }

        /// <summary>
        /// DbSet of QuestionsAnswers
        /// </summary>
        public DbSet<QuestionAnswer> QuestionsAnswers { get; set; }

        /// <summary>
        /// DbSet of results
        /// </summary>
        public DbSet<Result> Results { get; set; }

        /// <summary>
        /// Configuration for database.
        /// </summary>
        /// <param name="optionsBuilder">optionsBuilder</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB; Database = Testing; Trusted_Connection=true");
            }
        }
    }
}
