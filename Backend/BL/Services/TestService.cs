﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.Interfaces;
using BL.Models;
using BL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    /// <summary>
    /// Test Service for test actions.
    /// </summary>
    public class TestService : ITest
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private IUnitOfWork DataBase { get; }

        /// <summary>
        /// Variable of IMapper type for data mapping.
        /// </summary>
        private IMapper Mapper { get; }

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">Variable for data mapping.</param>
        public TestService(IUnitOfWork uow, IMapper mapper)
        {
            DataBase = uow;
            Mapper = mapper;
        }

        /// <summary>
        /// Gets all the QuestionsAnswers Models.
        /// </summary>
        /// <returns>List of QuestionsAnswers models</returns>
        public IEnumerable<QuestionAnswerModel> GetAll()
        {
            return Mapper.Map<IEnumerable<QuestionAnswer>, List<QuestionAnswerModel>>(DataBase.QuestionAnswerRepository.FindAll().ToList());
        }

        /// <summary>
        /// Gets all not passed tests models.
        /// </summary>
        /// <returns>List of Tests models</returns>
        public IEnumerable<TestModel> GetAllNotPassedTests(int groupId, int studentId)
        {
            var results = DataBase.ResultRepository.FindAll().Where(r => r.StudentId == studentId).Select(x => x.TestId);
            var notPassedTests = DataBase.TestRepository.FindAll().Include(t => t.QuestionsAnswers).Where(y => !results.Contains(y.Id)).Where(t => t.GroupId == groupId);

            return Mapper.Map<IEnumerable<Test>, List<TestModel>>(notPassedTests);
        }

        /// <summary>
        /// Gets all results models with details.
        /// </summary>
        /// <returns>List of Result models with details</returns>
        public IEnumerable<ResultModel> GetAllResultWithDetails(int studentId)
        {
            return Mapper.Map<IEnumerable<Result>, List<ResultModel>>(DataBase.ResultRepository.FindAll().Include(r => r.Test).Where(r => r.StudentId == studentId));
        }

        /// <summary>
        /// Gets all teacher current tests (QuestionsAnswers models).
        /// </summary>
        /// <returns>List of QuestionsAnswers models</returns>
        public IEnumerable<QuestionAnswerModel> GetTeacherCurrentTests(string testName)
        {
            var test = DataBase.TestRepository.GetByAsync(t => t.Name == testName).Result;

            var qns = Mapper.Map<IEnumerable<QuestionAnswer>, List<QuestionAnswerModel>>(DataBase.QuestionAnswerRepository.FindAll().Where(q => q.TestId == test.Id));
            return qns.OrderByDescending(q => q.QnId);
        }

        /// <summary>
        /// Gets all tests models for teacher.
        /// </summary>
        /// <returns>List of Tests models</returns>
        public IEnumerable<TestModel> GetAllTestsForTeacher(int teacherId)
        {
            var tests = DataBase.TestRepository.FindAll().Include(t => t.Results).Where(t => t.TeacherId == teacherId);

            var testsModel = Mapper.Map<IEnumerable<Test>, List<TestModel>>(tests);

            return testsModel;
        }

        /// <summary>
        /// Gets all teacher tests models with details.
        /// </summary>
        /// <returns>List of Tests models</returns>
        public IEnumerable<TestModel> GetTeacherTestsWithDetails(int teacherId)
        {
            var tests = DataBase.TestRepository.FindAll().Include(t => t.QuestionsAnswers).Include(t=>t.Group).Where(t => t.TeacherId == teacherId);

            var testsModel = Mapper.Map<IEnumerable<Test>, List<TestModel>>(tests);

            return testsModel;
        }

        /// <summary>
        /// Gets all result models for teacher.
        /// </summary>
        /// <returns>List of Tests models</returns>
        public IEnumerable<ResultModel> GetResultsForTeacher(int testId)
        {
            var results = DataBase.ResultRepository.FindAll().Include(t => t.Student.User).Where(t => t.TestId == testId).ToListAsync();

            var resultsModel = Mapper.Map<IEnumerable<Result>, List<ResultModel>>(results.Result);

            return resultsModel;
        }

        /// <summary>
        /// Adds result to database through Result Repository async.
        /// </summary>
        /// <param name="model">Result Model</param>
        public async Task AddResult(ResultModel model)
        {
            var result = Mapper.Map<Result>(model);

            await DataBase.ResultRepository.AddAsync(result);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Adds group for test to database through Test Repository async.
        /// </summary>
        /// <param name="model">Group Model</param>
        /// <param name="groupName">Name of current group</param>
        public async Task<TestModel> AddTestName(TestModel model, string groupName)
        {
            if (string.IsNullOrEmpty(model.Name) || string.IsNullOrEmpty(groupName))
            {
                throw new TestingException("This fields Test name or Group are empty!", "");
            }

            if (DataBase.TestRepository.FindAll().Any(t => t.Name == model.Name))
            {
                throw new TestingException("This test is already exist in database!", "");
            }

            var group = await DataBase.GroupRepository.GetByAsync(g => g.Name == groupName);

            if (group == null)
            {
                throw new TestingException("Group was not found!", "");
            }

            model.GroupId = group.Id;

            var result = Mapper.Map<Test>(model);

            await DataBase.TestRepository.AddAsync(result);

            await DataBase.SaveAsync();

            return model;
        }

        /// <summary>
        /// Adds test for questionAnswer to database through Test Repository async.
        /// </summary>
        /// <param name="model">QuestionAnswer Model</param>
        /// <param name="testName">Name of current test</param>
        public async Task<QuestionAnswerModel> AddTest(QuestionAnswerModel model, string testName)
        {
            if (string.IsNullOrEmpty(model.Qn))
            {
                throw new TestingException("The qn is not specified!", "");
            }

            if (string.IsNullOrEmpty(model.Option1))
            {
                throw new TestingException("The option 1 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option2))
            {
                throw new TestingException("The option 2 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option3))
            {
                throw new TestingException("The option 3 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option4))
            {
                throw new TestingException("The option 4 is not specified", "");
            }

            if (model.Answer < 0 || model.Answer > 3)
            {
                throw new TestingException("The answer field is incorrect!", "");
            }

            var test = await DataBase.TestRepository.GetByAsync(t => t.Name == testName);

            if (test == null)
            {
                throw new TestingException("Test was not found!", "");
            }

            var qA = await DataBase.QuestionAnswerRepository.GetByAsync(qA => qA.Qn == model.Qn && qA.TestId == test.Id);

            if (qA != null)
            {
                throw new TestingException("This question already exists for this test!", "");
            }

            var questionAnswer = Mapper.Map<QuestionAnswer>(model);

            questionAnswer.Test = test;

            await DataBase.QuestionAnswerRepository.AddAsync(questionAnswer);

            await DataBase.SaveAsync();

            var resultModel = await DataBase.QuestionAnswerRepository.GetByAsync(q =>
                q.Qn == questionAnswer.Qn && q.TestId == questionAnswer.TestId);

            var res = Mapper.Map<QuestionAnswerModel>(resultModel);

            return res;
        }

        /// <summary>
        /// Gets question answer model from Group Repository by id async.
        /// </summary>
        /// <param name="id">Group id</param>
        /// <returns>Group model</returns>
        public async Task<QuestionAnswerModel> GetByIdAsync(int id)
        {
            return Mapper.Map<QuestionAnswerModel>(await DataBase.QuestionAnswerRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Adds QuestionAnswer to database through Teacher Repository async.
        /// </summary>
        /// <param name="model">QuestionAnswer Model</param>
        public async Task AddAsync(QuestionAnswerModel model)
        {
            if (string.IsNullOrEmpty(model.Qn))
            {
                throw new TestingException("The qn is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option1))
            {
                throw new TestingException("The option 1 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option2))
            {
                throw new TestingException("The option 2 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option3))
            {
                throw new TestingException("The option 3 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option4))
            {
                throw new TestingException("The option 4 is not specified", "");
            }

            if (model.Answer < 0 || model.Answer > 3)
            {
                throw new TestingException("The answer is incorrect", "");
            }

            var questionAnswer = Mapper.Map<QuestionAnswer>(model);

            await DataBase.QuestionAnswerRepository.AddAsync(questionAnswer);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Updates QuestionAnswer in database through Question Answer Repository async.
        /// </summary>
        /// <param name="model">Question Answer model</param>
        public async Task UpdateAsync(QuestionAnswerModel model)
        {
            if (string.IsNullOrEmpty(model.Qn))
            {
                throw new TestingException("The qn is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option1))
            {
                throw new TestingException("The option 1 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option2))
            {
                throw new TestingException("The option 2 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option3))
            {
                throw new TestingException("The option 3 is not specified", "");
            }

            if (string.IsNullOrEmpty(model.Option4))
            {
                throw new TestingException("The option 4 is not specified", "");
            }

            var questionAnswerCheck = await DataBase.QuestionAnswerRepository.GetByAsync(q => q.Qn == model.Qn && q.Option1 == model.Option1 && q.Option2 == model.Option2 && q.Option3 == model.Option3 && q.Option4 == model.Option4 && q.TestId == model.TestId);

            if (questionAnswerCheck != null)
            {
                throw new TestingException("This question already exists for this test!", "");
            }

            var qA = Mapper.Map<QuestionAnswer>(model);

            DataBase.QuestionAnswerRepository.Update(qA);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Updates test in database through Test Repository async.
        /// </summary>
        /// <param name="testModel">Test model</param>
        public async Task UpdateTestAsync(TestModel testModel)
        {
            if (string.IsNullOrEmpty(testModel.Name))
            {
                throw new TestingException("The author is not specified", "");
            }

            var testCheck = await DataBase.TestRepository.GetByAsync(t => t.Name == testModel.Name);

            if (testCheck != null)
            {
                throw new TestingException($"This test name: \"{testCheck.Name}\" already exist!", "");
            }

            var test = Mapper.Map<Test>(testModel);

            DataBase.TestRepository.Update(test);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Deletes test in database by modelId through Test Repository async.
        /// </summary>
        /// <param name="modelId">Test Model id</param>
        public async Task DeleteByIdAsync(int modelId)
        {
            await DataBase.TestRepository.DeleteByIdAsync(modelId);
            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Deletes questionAnswer in database by modelId through QuestionAnswer Repository async.
        /// </summary>
        /// <param name="modelId">Question Answer Model id</param>
        public async Task DeleteByIdQuestionAnswerAsync(int modelId)
        {
            await DataBase.QuestionAnswerRepository.DeleteByIdAsync(modelId);
            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Receives all questions and answer options for a particular test through its id.
        /// </summary>
        /// <param name="id">Id of current test</param>
        /// <returns>A list of objects consisting of a question and answer options.</returns>
        public IEnumerable<object> GetQuestionsByTestId(int id)
        {
            var questions = Mapper.Map<IEnumerable<QuestionAnswer>, List<QuestionAnswerModel>>(DataBase.QuestionAnswerRepository.FindAll().ToList());

            var qns = questions.Where(q => q.TestId == id)
                .Select(x => new
                { QnId = x.QnId, Qn = x.Qn, x.Option1, x.Option2, x.Option3, x.Option4 })
                .OrderBy(y => Guid.NewGuid())
                .Take(10)
                .ToArray();

            var updated = qns.AsEnumerable()
                .Select(x => new
                {
                    QnId = x.QnId,
                    Qn = x.Qn,
                    Options = new string[] { x.Option1, x.Option2, x.Option3, x.Option4 }
                }).ToList();

            return updated;
        }

        /// <summary>
        /// Gets all responses for a further comparison.
        /// </summary>
        /// <param name="qIDs">User answers</param>
        /// <param name="id">Test id</param>
        /// <returns>Array of correct answers</returns>
        public int[] GetAnswers(int[] qIDs, int id)
        {
            var questions = Mapper.Map<IEnumerable<QuestionAnswer>, List<QuestionAnswerModel>>(DataBase.QuestionAnswerRepository.FindAll().ToList());

            var result = questions.Where(q => q.TestId == id)
                .AsEnumerable()
                .Where(y => qIDs.Contains(y.QnId))
                .OrderBy(x => Array.IndexOf(qIDs, x.QnId))
                .Select(z => z.Answer)
                .ToArray();

            return result;
        }
    }
}