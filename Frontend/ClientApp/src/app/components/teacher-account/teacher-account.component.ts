import { Student } from '../../interfaces/Student';
import { User } from '../../interfaces/User';
import { BaseUser } from '../../interfaces/BaseUser';
import { Group } from '../../interfaces/Group';
import { StudentService } from 'src/app/services/student.service';
import { GroupService } from 'src/app/services/group.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';
import { TestService } from 'src/app/services/test.service';
import { Teacher } from 'src/app/interfaces/Teacher';
import { QuestionAnswer } from 'src/app/interfaces/QuestionAnswer';
import { AssertNotNull } from '@angular/compiler';

@Component({
  selector: 'app-students',
  templateUrl: './teacher-account.component.html',
  styleUrls: ['./teacher-account.component.css']
})
export class TeacherAccountComponent implements OnInit {

  currentUser = this.auth.getCurrentUserValue();
  users: BaseUser[] = [];
  user: BaseUser = new BaseUser();

  tableMode = true;
  tableMode2 = true;
  groupManager = false;
  groups: Group[] = [];
  group: Group = new Group();
  mainManager = true;
  userManager = false;
  teacher: Teacher;

  curId: number;

  isMain: boolean = true;
  isAddTestManager: boolean;
  isTestManager: boolean;
  isShowAllResults: boolean;

  isEditMode: boolean = true;

  currentTests: any[] = [];

  isAddTests: boolean;

  showRes: boolean = true;

  testsForResult: any[];
  results: any[];
  tests: any[];
  test: any;
  questionAnswer: QuestionAnswer = new QuestionAnswer();
  questionsAnswers: QuestionAnswer[];

  curTest: any;

  curSelectedTest: any;

  constructor(private studentService: StudentService,
    private groupService: GroupService,
    private auth: AuthService,
    public testService: TestService) { }

  students: User[] = [];
  teachers: User[] = [];
  student: User;
  counts: number;
  myError: string;
  testName: string;
  groupName: string;

  testForm: FormGroup;

  ngOnInit(): void {
    this.myError = '';

    this.testForm = new FormGroup({
      qn: new FormControl('', Validators.minLength(1)),
      option1: new FormControl('', Validators.minLength(1)),
      option2: new FormControl('', Validators.minLength(1)),
      option3: new FormControl('', Validators.minLength(1)),
      option4: new FormControl('', Validators.minLength(1)),
      answer: new FormControl('', Validators.required)
    });

    this.studentService.getTeacher(this.currentUser.id).subscribe((teacher) => {
      this.teacher = teacher;
      this.testService.teacherId = this.teacher.id;
      this.testService.getTeacherTests().subscribe((tests) => {
        this.testsForResult = tests;
        console.log("testsForResult");
        console.log(this.testsForResult);
      });
    });

    this.studentService.getStudents().subscribe((data) => {
      this.students = data;
    });

    this.studentService.getTeachers().subscribe((data) => {
      console.log(data);
      this.teachers = data;
    });

    this.studentService.getBaseUsers().subscribe((data) => {
      this.users = data;
    });

    this.groupService.getGroups().subscribe((data) => {
      this.groups = data;
    });

  }

  login(): void {
    console.log(this.testForm.value);
  }

  addTestName(): void {
    this.testService.testName = this.testName;
    localStorage.setItem('testName', this.testName);
    this.testService.addTestName(this.groupName).subscribe(
      () => {
        this.isAddTests = true;
        localStorage.setItem('isAddTests', "true");
        this.myError = '';
      },
      (exc) => {
        this.myError = exc.error;
      }
    );
  }

  addTest() {
    console.log("add test is work!!!!");
    this.testService.addTest(this.testForm.value).subscribe(
      () => {
        this.testForm.reset();
        Object.keys(this.testForm.controls).forEach(key => {
          this.testForm.controls[key].setErrors(null);
        });
        this.testService.getTeacherCurrentTests().subscribe(
          (data) => {
            this.currentTests = data;
            console.log("currentTests:");
            console.log(this.currentTests);
          }
        );
        this.myError = '';
      },
      (exc) => {
        this.myError = exc.error;
      }
    );
  }

  goToMain() {
    this.isMain = true;
    this.isAddTestManager = false;
    this.isTestManager = false;
    this.isShowAllResults = false;
  }

  testManager() {
    this.isMain = false;
    this.isAddTestManager = false;
    this.isTestManager = true;
    this.isShowAllResults = false;
    this.getTeacherTests2();
  }

  addTestManager() {
    this.isMain = false;
    this.isAddTestManager = true;
    this.isTestManager = false;
    this.isShowAllResults = false;
  }

  showAllResults(): void {
    this.isMain = false;
    this.isAddTestManager = false;
    this.isTestManager = false;
    this.isShowAllResults = true;
  }

  ShowTestResult(id: number, testName: string): void {
    this.curSelectedTest = testName;
    console.log("result works");
    this.testService.testId = id;
    this.testService.getResultsForTeacher().subscribe((results) => {
      this.results = results;
      console.log("Results is");
      console.log(this.results);
      if (this.results.length === 0) {
        this.showRes = false;
      }
      else {
        this.showRes = true;
      }
    });
  }

  getTeacherTests2(): void {
    this.testService.getTeacherTests().subscribe((tests) => {
      this.tests = tests;
      console.log("tests:");
      console.log(this.tests);
    });
  }

  updateTest() {
    console.log("updateTest1Work");
    this.testService.updateTest(this.test)
      .subscribe(data => {
        this.testService.getTeacherTests().subscribe((tests) => {
          this.tests = tests;
        });
        this.testService.getTeacherTests().subscribe((tests) => {
          this.testsForResult = tests;
        });
        this.myError = '';
      }, (exc) => {
        this.myError = exc.error;
      });
    this.cancelTest();
  }

  editTest(t: any): void {
    this.test = t;
  }

  cancelTest(): void {
    this.test = null;
    this.tableMode = true;
  }

  deleteTest(t: Group): void {
    this.testService.deleteTest(t.id)
      .subscribe(data => {
        this.testService.getTeacherTests().subscribe((tests) => {
          this.tests = tests;
        });
      });
  }

  openEditTest(test: any): void {
    console.log("Test is");
    console.log(test);
    this.curTest = test;
    this.testService.testName = test.name;
    this.questionsAnswers = test.questionsAnswers;
    console.log("this qa");
    console.log(this.questionsAnswers);
    this.isEditMode = false;
  }

  backToTestManager() {
    this.isEditMode = true;
  }

  addQuestionAnswer(): void {
    this.cancelQuestionAnswer();
    this.tableMode2 = false;
  }

  saveQuestionAnswer(): void {
    console.log("work1");
    if (this.questionAnswer.qnId == null) {
      this.testService.addTest(this.questionAnswer)
        .subscribe((data: any) => {
          this.questionsAnswers.push(data);
          this.myError = '';
        }, (exc) => {
          this.myError = exc.error;
        });
    } else {
      console.log("work2");
      this.testService.updateQuestionAnswer(this.questionAnswer)
        .subscribe(data => {
          this.testService.getTeacherTests().subscribe((tests) => {
            this.tests = tests;
          });
          this.myError = '';
        }, (exc) => {
          this.myError = exc.error;
        });
    }
    this.cancelQuestionAnswer();
  }

  cancelQuestionAnswer(): void {
    this.questionAnswer = new QuestionAnswer();
    this.tableMode2 = true;
  }

  editQuestionAnswer(qa: QuestionAnswer): void {
    this.questionAnswer = qa;
  }

  deleteQuestionAnswer(qa: QuestionAnswer): void {
    console.log("Delete!");
    console.log(qa);

    this.testService.deleteQuestionAnswer(qa.qnId)
      .subscribe(data => {
        this.testService.getTeacherTests().subscribe((tests) => {
          this.tests = tests;

          for (let index = 0; index < tests.length; index++) {
            if (tests[index].id == this.curTest.id) {
              this.curId = index;
            }
          }
          this.openEditTest(tests[this.curId]);
        });
      });
  }
}
