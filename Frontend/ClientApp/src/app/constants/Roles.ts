export class Roles {
  static readonly Admin = 'Admin';
  static readonly Student = 'Student';
  static readonly Teacher = 'Teacher';
}
