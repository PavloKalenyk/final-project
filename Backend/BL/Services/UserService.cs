﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.Common;
using BL.Constants;
using BL.Interfaces;
using BL.Models;
using BL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BL.Services
{
    public class UserService : IUserService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private IUnitOfWork DataBase { get; }

        /// <summary>
        /// Variable of IMapper type for data mapping.
        /// </summary>
        private IMapper Mapper { get; }

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">Variable for data mapping.</param>
        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            DataBase = uow;
            Mapper = mapper;
        }

        /// <summary>
        /// Gets all the User Models.
        /// </summary>
        /// <returns>List of User models</returns>
        public IEnumerable<UserModel> GetAll()
        {
            return Mapper.Map<IEnumerable<User>, List<UserModel>>(DataBase.UserRepository.FindAll());
        }

        /// <summary>
        /// Gets all the users with details models.
        /// </summary>
        /// <returns>List of Users models</returns>
        public IEnumerable<UserModel> GetAllWithDetails()
        {
            return Mapper.Map<IEnumerable<User>, List<UserModel>>(DataBase.UserRepository.FindAll().Include(u=>u.Student).ToList());
        }

        /// <summary>
        /// Gets user models with details from User Repository by id async.
        /// </summary>
        /// <param name="id">Group id</param>
        /// <returns>Group model</returns>
        public async Task<UserModel> GetByIdAsync(int id)
        {
            return Mapper.Map<UserModel>(await DataBase.UserRepository.GetByIdAsync(id));
        }
            
        /// <summary>
        /// Adds user model to database through User Repository async.
        /// </summary>
        /// <param name="model">User Model</param>
        public async Task AddAsync(UserModel model)
        {
            if (string.IsNullOrEmpty(model.Login))
            {
                throw new TestingException("You must input Login field!", "");
            }

            if (string.IsNullOrEmpty(model.FirstName))
            {
                throw new TestingException("The first name field is empty!", "");
            }

            if (string.IsNullOrEmpty(model.LastName))
            {
                throw new TestingException("The last name field is empty!", "");
            }

            if (string.IsNullOrEmpty(model.Password))
            {
                throw new TestingException("The password field is empty!", "");
            }

            if (string.IsNullOrEmpty(model.Role))
            {
                throw new TestingException("The role field is empty!", "");
            }

            if (model.Role == "Student" && string.IsNullOrEmpty(model.GroupName))
            {
                throw new TestingException("The group field is empty!", "");
            }

            var user = await DataBase.UserRepository.GetByAsync(u => u.Login == model.Login);

            if (user != null)
            {
                throw new TestingException("User " + user.Login + " exist in database", "");
            }

            var group = await DataBase.GroupRepository.GetByAsync(g => g.Name == model.GroupName);

            await DataBase.UserRepository.AddAsync(new User
            {
                Login = model.Login,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Role = model.Role,
                IsActivate = model.IsActivate,
                Password = EncryptAndDecrypt.ConvertToEncrypt(model.Password)
            });

            var newUser = await DataBase.UserRepository.GetByAsync(u => u.Login == model.Login);

            switch (newUser.Role)
            {
                case Roles.Student:
                    await DataBase.StudentRepository.AddAsync(new Student
                    {
                        User = newUser,
                        Group = @group
                    });
                    break;
                case Roles.Teacher:
                    await DataBase.TeacherRepository.AddAsync(new Teacher
                    {
                        User = newUser
                    });
                    break;
            }
        }

        /// <summary>
        /// Updates user in database through User Repository async.
        /// </summary>
        /// <param name="model">User model</param>
        public async Task UpdateAsync(UserModel model)
        {
            if (string.IsNullOrEmpty(model.Login))
            {
                throw new TestingException("You must input Login field!", "");
            }

            if (string.IsNullOrEmpty(model.FirstName))
            {
                throw new TestingException("The first name field is empty!", "");
            }

            if (string.IsNullOrEmpty(model.LastName))
            {
                throw new TestingException("The last name field is empty!", "");
            }

            var userCheck = await DataBase.UserRepository.GetByAsync(u => u.Login == model.Login && u.IsActivate == true);

            if (userCheck != null)
            {
                throw new TestingException("User " + userCheck.Login + " exist in database", "");
            }

            var user = Mapper.Map<User>(model);

            DataBase.UserRepository.Update(user);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Deletes user in database by modelId through User Repository async.
        /// </summary>
        /// <param name="modelId">User Model id</param>
        public async Task DeleteByIdAsync(int modelId)
        {
            await DataBase.UserRepository.DeleteByIdAsync(modelId);
            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Gets user model from database through User Repository by async.
        /// </summary>
        /// <param name="model">User Model</param>
        /// <returns>User Model</returns>
        public async Task<UserModel> GetByAsync(UserModel model)
        {
            var group = await DataBase.UserRepository.GetByAsync(u => u.Login == model.Login);

            return Mapper.Map<UserModel>(group);
        }

        /// <summary>
        /// Gets all Gets all non-activated students.
        /// </summary>
        /// <returns>List of non-activated students</returns>
        public IEnumerable<UserModel> GetStudents()
        {
            return Mapper.Map<IEnumerable<User>, List<UserModel>>(DataBase.UserRepository.FindAll().Where(u => u.Role == "Student" && u.IsActivate == false));
        }

        /// <summary>
        /// Gets all Gets all non-activated teachers.
        /// </summary>
        /// <returns>List of non-activated teachers</returns>
        public IEnumerable<UserModel> GetTeachers()
        {
            return Mapper.Map<IEnumerable<User>, List<UserModel>>(DataBase.UserRepository.FindAll().Where(u => u.Role == "Teacher" && u.IsActivate == false));
        }

        /// <summary>
        /// Gets all users with the student or teacher role.
        /// </summary>
        /// <returns>List of all users with the student or teacher role</returns>
        public IEnumerable<UserModel> GetBaseUsers()
        {
            return Mapper.Map<IEnumerable<User>, List<UserModel>>(DataBase.UserRepository.FindAll().Where(u => (u.Role == "Student" || u.Role == "Teacher") && u.IsActivate));
        }
    }
}