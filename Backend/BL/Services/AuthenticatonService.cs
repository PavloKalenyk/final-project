﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BL.Common;
using BL.Constants;
using BL.Helpers;
using BL.Interfaces;
using BL.Models;
using BL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BL.Services
{
    /// <summary>
    /// Authentication Service for user authentication and registration.
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private IUnitOfWork DataBase { get; }

        /// <summary>
        /// Variable for accessing the secrete key.
        /// </summary>
        private readonly AppSettings _appSettings;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="appSettings">Variable for accessing the secrete key.</param>
        public AuthenticationService(IUnitOfWork uow, IOptions<AppSettings> appSettings)
        {
            DataBase = uow;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Authenticates a user.
        /// </summary>
        /// <param name="userAuthModel">UserAuthModel from client</param>
        /// <returns> UserAuthModel to client </returns>
        public async Task<UserLoggedModel> Authenticate(UserAuthModel userAuthModel)
        {
            if (string.IsNullOrEmpty(userAuthModel.Login))
            {
                throw new TestingException("The login field is empty!", "");
            }

            if (string.IsNullOrEmpty(userAuthModel.Password))
            {
                throw new TestingException("The password field is empty!", "");
            }

            var user = await DataBase.UserRepository.GetByAsync(u => u.Login == userAuthModel.Login);

            if (user == null)
                throw new TestingException("User with this login is not registered!", "");

            var password = EncryptAndDecrypt.ConvertToDecrypt(user.Password);

            if (password != userAuthModel.Password)
            {
                throw new TestingException("Incorrect password. Please try again!", "");
            }

            // Authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            
            UserLoggedModel userLoggedModel = new UserLoggedModel { Id = user.Id, Login = user.Login, FirstName = user.FirstName, LastName = user.LastName, Role = user.Role, IsActivate = user.IsActivate, Token = tokenHandler.WriteToken(token) };

            return userLoggedModel;
        }

        /// <summary>
        /// Registers a user.
        /// </summary>
        /// <param name="userRegisterModel"></param>
        /// <returns></returns>
        public async Task RegisterAsync(UserRegisterModel userRegisterModel)
        {
            if (string.IsNullOrEmpty(userRegisterModel.Login))
            {
                throw new TestingException("You must input Login field!", "");
            }

            if (string.IsNullOrEmpty(userRegisterModel.FirstName))
            {
                throw new TestingException("The first name field is empty!", "");
            }

            if (string.IsNullOrEmpty(userRegisterModel.LastName))
            {
                throw new TestingException("The last name field is empty!", "");
            }

            if (string.IsNullOrEmpty(userRegisterModel.Login))
            {
                throw new TestingException("The login field is empty!", "");
            }

            if (string.IsNullOrEmpty(userRegisterModel.Password))
            {
                throw new TestingException("The password field is empty!", "");
            }

            var user = await DataBase.UserRepository.GetByAsync(u => u.Login == userRegisterModel.Login);

            if (user != null)
            {
                throw new TestingException("User " + user.Login + " exist in database", "");
            }

            var group = await DataBase.GroupRepository.GetByAsync(g => g.Name == userRegisterModel.GroupName);

            await DataBase.UserRepository.AddAsync(new User
            {
                Login = userRegisterModel.Login,
                FirstName = userRegisterModel.FirstName,
                LastName = userRegisterModel.LastName,
                Role = userRegisterModel.Role,
                Password = EncryptAndDecrypt.ConvertToEncrypt(userRegisterModel.Password)
            });

            var newUser = await DataBase.UserRepository.GetByAsync(u => u.Login == userRegisterModel.Login);

            switch (newUser.Role)
            {
                case Roles.Student:
                    await DataBase.StudentRepository.AddAsync(new Student
                    {
                        User = newUser,
                        Group = @group
                    });
                    break;
                case Roles.Teacher:
                    await DataBase.TeacherRepository.AddAsync(new Teacher
                    {
                        User = newUser
                    });
                    break;
            }
        }
    }
}