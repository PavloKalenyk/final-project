﻿using System.Collections.Generic;

namespace BL.Models
{
    public class TestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }
        public GroupModel Group { get; set; } 
        public int TeacherId { get; set; }
        public TeacherModel Teacher { get; set; }
        public List<QuestionAnswerModel> QuestionsAnswers { get; set; }
        public List<ResultModel> Results { get; set; }
    }
}