﻿using System;
using System.Runtime.Serialization;

namespace BL.Validation
{
    [Serializable]
    public class TestingException : Exception
    {
        public string Property { get; protected set; }

        public TestingException(string message, string prop) : base(message)
        {
            Property = prop;
        }

        protected TestingException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}