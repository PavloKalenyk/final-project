import { AuthService } from './services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title = "Knowledge Testing System";
  public login: string;
  isAuth = false;

  constructor(private studentService: StudentService, private authService: AuthService, private router: Router) {
    this.authService.currentUser.subscribe(user => {
      if (user) {
        this.isAuth = true;
        this.login = user.login;
      }
      else {
        this.isAuth = false;
        this.login = '';
      }
    });
  }

  ngOnInit() {
    console.log("CurrentUser");
    console.log(this.authService.getCurrentUserValue());

    this.studentService.getStudents().subscribe((data) => {
      console.log("STUDENTS:");
      console.log(data);
    });

    let currentUser = this.authService.getCurrentUserValue();
    this.isAuth = currentUser == null ? false : true;
    this.login = currentUser == null ? '' : currentUser.login;
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
