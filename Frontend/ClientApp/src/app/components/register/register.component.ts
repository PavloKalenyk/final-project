import { UserRegister } from './../../interfaces/user-register';
import { AuthService } from './../../services/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GroupService } from './../../services/group.service';
import { Group } from '../../interfaces/Group';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router, private groupService: GroupService) { }

  // checkStudForm: FormGroup;
  registerForm: FormGroup;
  userId: number;
  isIdReceived: boolean;
  checkError: string;
  registerError: string;
  isGroup: boolean;
  groups: Group[] = [];

  ngOnInit(): void {
    // this.checkStudForm = new FormGroup({
    //   name: new FormControl(''),
    //   surname: new FormControl('')
    // });

    this.groupService.getGroups().subscribe((data) => {
      this.groups = data;
      console.log(data);
    });

    this.isGroup = false;
    this.registerForm = new FormGroup({
      login: new FormControl(''),
      password: new FormControl(''),
      firstName: new FormControl('', Validators.minLength(3)),
      lastName: new FormControl('', Validators.minLength(3)),
      groupName: new FormControl(''),
      role: new FormControl('')
    });
    this.isIdReceived = true;
    this.checkError = '';
    this.registerError = '';
  }

  roleChoose($event) {
    console.log($event);
    if ($event === "Student") {
      this.isGroup = true;
    }
    else {
      this.isGroup = false;
    }
  }

  register() {
    let userRegister = this.registerForm.value;

    console.log(userRegister['groupName']);

    userRegister['userId'] = this.userId;

    this.authService.register(userRegister).subscribe(
      () => {
        this.router.navigate(['/login']);
      },
      (exc) => {
        this.registerError = exc.error;
      }
    );
  }

}
