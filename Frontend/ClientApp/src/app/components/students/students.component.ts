import { Student } from '../../interfaces/Student';
import { User } from '../../interfaces/User';
import { BaseUser } from '../../interfaces/BaseUser';
import { Group } from '../../interfaces/Group';
import { StudentService } from 'src/app/services/student.service';
import { GroupService } from 'src/app/services/group.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  users: BaseUser[] = [];
  user: BaseUser = new BaseUser();

  tableMode = true;
  tableMode2 = true;

  mainManager = true;
  groupManager = false;
  groups: Group[] = [];
  group: Group = new Group();
  userManager = false;

  isGroup: boolean;

  constructor(private studentService: StudentService,
    private groupService: GroupService,
    private formBuilder: FormBuilder) { }

  students: User[] = [];
  teachers: User[] = [];

  student: User;
  myError = '';

  addStudentForm = this.formBuilder.group({
    firstName: ['', [Validators.required, Validators.minLength(1)]],
    lastName: ['', [Validators.required, Validators.minLength(1)]],
    login: ['', [Validators.required, Validators.minLength(1)]],
    password: ['', [Validators.required, Validators.minLength(1)]],
    role: ['', [Validators.required, Validators.minLength(1)]]
  });

  ngOnInit(): void {
    this.isGroup = false;
    this.studentService.getStudents().subscribe((data) => {
      console.log(data);
      this.students = data;
      this.studentService.getBaseUsers().subscribe((baseUsers) => {
        console.log("baseUsers");
        this.users = baseUsers;
        console.log(this.users);
      });
    });

    this.studentService.getTeachers().subscribe((data) => {
      console.log(data);
      this.teachers = data;
      this.studentService.getBaseUsers().subscribe((baseUsers) => {
        console.log("baseUsers");
        this.users = baseUsers;
        console.log(this.users);
      });
    });

    this.groupService.getGroups().subscribe((data) => {
      this.groups = data;
    });
  }

  addStudent(student: User): void {
    student.isActivate = true;
    this.studentService.addUser(student).subscribe((data: any) => {
      console.log(data);
      this.studentService.getStudents().subscribe((students) => {
        this.students = students;
        console.log(students);
      });
    },
      (error) => { console.log(error); }
    );
  }

  deleteStudent(studentId: number): void {
    this.studentService.deleteStudent(studentId)
      .subscribe((data) => {
        console.log(data);
        this.studentService.getStudents().subscribe((students) => {
          this.students = students;
          console.log(data);
        });
        this.studentService.getTeachers().subscribe((teachers) => {
          this.teachers = teachers;
        });
      });
  }

  apply(user: User): void {
    user["isActivate"] = true;
    this.studentService.updateUser(user).subscribe((data) => {
      this.studentService.getStudents().subscribe((students) => {
        this.students = students;
      });
      this.studentService.getTeachers().subscribe((teachers) => {
        this.teachers = teachers;
      });
      this.studentService.getBaseUsers().subscribe((baseUsers) => {
        this.users = baseUsers;
      });
    });
  }

  activeUsersManager(): void {
    this.userManager = true;
    this.groupManager = false;
    this.mainManager = false;
  }

  activeGroupManager(): void {
    this.groupManager = true;
    this.userManager = false;
    this.mainManager = false;
  }

  goToMain(): void {
    this.mainManager = true;
    this.groupManager = false;
    this.userManager = false;
  }

  saveGroup(): void {
    console.log("Group id:");
    console.log(this.group.id);
    if (this.group.id == null) {
      this.groupService.addGroup(this.group)
        .subscribe((data: Group) => {
          this.groups.push(data);
          console.log("groupsssss");
          console.log(this.groups);
          this.myError = '';
        }, (exc) => {
          this.myError = exc.error;
        });
    } else {
      console.log("work");
      this.groupService.updateGroup(this.group)
        .subscribe(data => {
          this.groupService.getGroups().subscribe((groups) => {
            console.log("groups are");
            this.groups = groups;
            console.log(this.groups);
            this.myError = '';
          });
        }, (exc) => {
          this.myError = exc.error;
          this.groupService.getGroups().subscribe((groups) => {
            this.groups = groups;
          });
        });
    }
    this.cancelGroup();
  }

  editGroup(g: Group): void {
    this.group = g;
  }

  cancelGroup(): void {
    this.group = new Group();
    this.tableMode = true;
  }

  deleteGroup(g: Group): void {
    this.groupService.deleteGroup(g.id)
      .subscribe(data => {
        this.groupService.getGroups().subscribe((groups) => {
          this.groups = groups;
          this.myError = '';
        });
      }, (exc) => {
        this.myError = exc.error;
      });
  }

  addGroup(): void {
    this.cancelGroup();
    this.tableMode = false;
  }

  saveUser(): void {
    console.log("User id:");
    console.log(this.user.id);
    this.user["isActivate"] = true;
    if (this.user.id == null) {
      this.studentService.addBaseUser(this.user)
        .subscribe((data: BaseUser) => {
          console.log("Base user is");
          console.log(data);
          this.users.push(data);
          console.log("This users is");
          console.log(this.users);
          this.myError = '';
        }, (exc) => {
          this.myError = exc.error;
        });
      this.studentService.getBaseUsers().subscribe((users) => {
        this.users = users;
      });
    } else {
      this.studentService.updateBaseUser(this.user)
        .subscribe(data => {
          this.studentService.getBaseUsers().subscribe((users) => {
            this.users = users;
            this.myError = '';
          });
        }, (exc) => {
          this.myError = exc.error;
        });
    }
    this.cancelUser();
  }

  editUser(u: BaseUser): void {
    this.user = u;
  }

  cancelUser(): void {
    this.user = new BaseUser();
    this.tableMode2 = true;
  }

  deleteUser(u: BaseUser): void {
    this.studentService.deleteBaseUser(u.id)
      .subscribe(data => {
        this.studentService.getBaseUsers().subscribe((users) => {
          this.users = users;
        });
      });
  }

  addUser(): void {
    this.cancelUser();
    this.tableMode2 = false;
  }

  roleChoose($event) {
    console.log($event);
    if ($event === "Student") {
      this.user.role = "Student";
      this.isGroup = true;
    }
    else {
      this.user.role = "Teacher";
      this.isGroup = false;
    }
  }
}
