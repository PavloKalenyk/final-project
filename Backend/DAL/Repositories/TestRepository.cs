﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TestRepository : ITestRepository
    {
        /// <summary>
        /// Object of TestingDbContext to access the database context.
        /// </summary>
        private readonly TestingDbContext _dataBase;

        /// <summary>
        /// Object instantiation.
        /// </summary>
        /// <param name="context">Object of TestingDbContext to access the database context.</param>
        public TestRepository(TestingDbContext context)
        {
            _dataBase = context;
        }

        /// <summary>
        /// Gets all tests in database.
        /// </summary>
        /// <returns>All tests entities in database</returns>
        public IQueryable<Test> FindAll()
        {
            return _dataBase.Tests;
        }

        /// <summary>
        /// Gets test from database by id async.
        /// </summary>
        /// <param name="id">id of test</param>
        /// <returns>Test entity from database</returns>
        public async Task<Test> GetByIdAsync(int id)
        {
            return await _dataBase.Tests.FindAsync(id);
        }

        /// <summary>
        /// Adds test entity to database.
        /// </summary>
        /// <param name="entity">Test entity</param>
        public async Task AddAsync(Test entity)
        {
            await _dataBase.Tests.AddAsync(entity);
            await _dataBase.SaveChangesAsync();
        }

        /// <summary>
        /// Updates test entity in database.
        /// </summary>
        /// <param name="entity">Test entity</param>
        public void Update(Test entity)
        {
            _dataBase.Entry(entity).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes test entity.
        /// </summary>
        /// <param name="entity">Test entity</param>
        public void Delete(Test entity)
        {
            _dataBase.Tests.Remove(entity);
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes test entity in database by id async.
        /// </summary>
        /// <param name="id">id of test entity</param>
        public async Task DeleteByIdAsync(int id)
        {
            Test test = await _dataBase.Tests.FindAsync(id);

            if (test != null)
            {
                _dataBase.Tests.Remove(test);
                await _dataBase.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Gets test entity from database by expression async.
        /// </summary>
        /// <param name="expression">Expression by which to search for the entity of the test.</param>
        /// <returns>Test Entity</returns>
        public async Task<Test> GetByAsync(Expression<Func<Test, bool>> expression)
        {
            return await _dataBase.Tests.FirstOrDefaultAsync(expression);
        }
    }
}