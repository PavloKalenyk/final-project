﻿using System.Threading.Tasks;
using BL.Models;

namespace BL.Interfaces
{
    public interface ITeacherService : ICrud<TeacherModel>
    {
        public Task<TeacherModel> GetByUserId(int id);
        public Task<TeacherModel> GetByAsync(TeacherModel model);

    }
}