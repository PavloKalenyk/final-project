﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BL.Models;

namespace BL.Interfaces
{
    public interface IUserService : ICrud<UserModel>
    {
        public Task<UserModel> GetByAsync(UserModel model);

        public IEnumerable<UserModel> GetAllWithDetails();

        public IEnumerable<UserModel> GetStudents();

        public IEnumerable<UserModel> GetTeachers();
        public IEnumerable<UserModel> GetBaseUsers();
    }
}