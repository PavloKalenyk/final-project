﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Constants;
using BL.Interfaces;
using BL.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestsController : ControllerBase
    {
        /// <summary>
        /// Variable of IGroupService type to access the Test Service.
        /// </summary>
        private readonly ITest _testService;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="testService">Variable of ITestService type to access the Test Service.</param>
        public TestsController(ITest testService)
        {
            _testService = testService;
        }

        // GET: api/<TestsController>/5
        [HttpGet("{id}")]
        public ObjectResult GetQuestionsByTestId(int id)
        {
            try
            {
                var questions = _testService.GetQuestionsByTestId(id);

                return Ok(questions);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<TestsController>/tests
        [HttpGet("tests")]
        public ObjectResult GetTestsByIds(int groupId, int studentId)
        {
            try
            {
                var tests = _testService.GetAllNotPassedTests(groupId, studentId);

                return Ok(tests);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<TestsController>/studentResults
        [HttpGet("studentResults")]
        public ObjectResult GetStudentResults(int studentId)
        {
            try
            {
                var tests = _testService.GetAllResultWithDetails(studentId);

                return Ok(tests);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<TestsController>/teacherCurrentTests
        [HttpGet("teacherCurrentTests")]
        public ObjectResult GetTeacherCurrentTests(string testName)
        {
            try
            {
                var curTests = _testService.GetTeacherCurrentTests(testName);

                return Ok(curTests);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<TestsController>/getAllResultsForTeacher
        [HttpGet("getAllResultsForTeacher")]
        public ObjectResult GetAllResultsForTeacher(int groupId, int studentId)
        {
            try
            {
                var tests = _testService.GetAllNotPassedTests(groupId, studentId);
                return Ok(tests);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<TestsController>/getTeacherTests
        [HttpGet("getTeacherTests")]
        public ObjectResult GetTeacherTests(int teacherId)
        {
            try
            {
                var tests = _testService.GetTeacherTestsWithDetails(teacherId);

                return Ok(tests);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<TestsController>/getResultsForTeacher
        [HttpGet("getResultsForTeacher")]
        public ObjectResult GetResultsForTeacher(int testId)
        {
            try
            {
                var tests = _testService.GetResultsForTeacher(testId);

                return Ok(tests);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<TestsController>/answers
        [HttpPost("answers")]
        public ObjectResult GetAnswers(int[] qIDs, int id)
        {
            try
            {
                var results = _testService.GetAnswers(qIDs, id);

                return Ok(results);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<TestsController>/result
        [HttpPost("result")]
        public Task AddResult(ResultModel resultModel)
        {
            try
            {
                return _testService.AddResult(resultModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<TestsController>/test
        [Authorize(Roles = Roles.Teacher)]
        [HttpPost("test")]
        public async Task<TestModel> AddTestName(TestModel testModel, string groupName)
        {
            try
            { 
                return await _testService.AddTestName(testModel, groupName);
            }
            catch (Exception e)
            { 
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<TestsController>/questionAnswer
        [Authorize(Roles = Roles.Teacher)]
        [HttpPost("questionAnswer")]
        public async Task<QuestionAnswerModel> AddTest([FromBody] QuestionAnswerModel questionAnswerModel, string testName)
        {
            try
            {
                return await _testService.AddTest(questionAnswerModel, testName);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<TestsController>/testModel
        [HttpPut]
        public async Task Put(TestModel testModel)
        {
            try
            {
                await _testService.UpdateTestAsync(testModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<TestsController>/putQuestionAnswer
        [HttpPut("putQuestionAnswer")]
        public async Task PutQuestionAnswer(QuestionAnswerModel questionAnswerModel)
        {
            try
            {
                await _testService.UpdateAsync(questionAnswerModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<TestsController>/5
        [HttpDelete("{testId}")]
        public async Task Delete(int testId)
        {
            try
            {
                await _testService.DeleteByIdAsync(testId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<TestsController>/deleteQa
        [HttpDelete("deleteQa")]
        public async Task DeleteQuestionAnswer(int qaId)
        {
            try
            {
                await _testService.DeleteByIdQuestionAnswerAsync(qaId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}