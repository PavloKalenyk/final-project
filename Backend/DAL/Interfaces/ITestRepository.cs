﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITestRepository : IRepository<Test>
    {
        public Task<Test> GetByAsync(Expression<Func<Test, bool>> expression);
    }
}