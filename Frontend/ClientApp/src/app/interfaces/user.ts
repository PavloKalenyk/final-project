import { Student } from './Student';

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  login: string;
  role: string;
  token: string;
  student: Student;
  isActivate: boolean;
}
