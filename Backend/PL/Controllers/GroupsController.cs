﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Constants;
using BL.Interfaces;
using BL.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        /// <summary>
        /// Variable of IGroupService type to access the Group Service.
        /// </summary>
        private readonly IGroupService _groupService;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="groupService">Variable of IGroupService type to access the Group Service.</param>
        public GroupsController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        // GET: api/<GroupsController>
        [HttpGet]
        public IEnumerable<GroupModel> Get()
        {
            try
            {
                var groups = _groupService.GetAll();

                return groups;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET api/<GroupsController>/5
        [HttpGet("{id}")]
        public async Task<GroupModel> Get(int id)
        {
            try
            {
                return await _groupService.GetByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<GroupsController>
        [Authorize(Roles = Roles.Admin)]
        [HttpPost]
        public async Task<GroupModel> Post([FromBody] GroupModel groupModel)
        {
            try
            { 
                await _groupService.AddAsync(groupModel);

                var group = await _groupService.GetByAsync(groupModel);

                return group;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<GroupsController>/5
        [Authorize(Roles = Roles.Admin)]
        [HttpPut]
        public async Task UpdateGroup(GroupModel groupModel)
        {
            try
            {
               await _groupService.UpdateAsync(groupModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<GroupsController>/5
        [Authorize(Roles = Roles.Admin)]
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            try
            {
                await _groupService.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
