﻿namespace BL.Models
{
    public class UserLoggedModel
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public bool IsActivate { get; set; }
    }
}