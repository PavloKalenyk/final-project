﻿using System.ComponentModel.DataAnnotations;

namespace BL.Models
{
    public class QuestionAnswerModel
    {
        public int QnId { get; set; }
        public string Qn { get; set; }
        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public string Option3 { get; set; }
        public string Option4 { get; set; }
        public int Answer { get; set; }

        public int? TestId { get; set; }
        public TestModel TestModel { get; set; }
    }
}