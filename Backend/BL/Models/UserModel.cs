﻿namespace BL.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool IsActivate { get; set; }
        public string GroupName { get; set; }
        public StudentModel Student { get; set; }
        public TeacherModel Teacher { get; set; }
    }
}