﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.Interfaces;
using BL.Models;
using DAL.Entities;
using DAL.Interfaces;

namespace BL.Services
{
    public class TeacherService : ITeacherService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private IUnitOfWork DataBase { get; }

        /// <summary>
        /// Variable of IMapper type for data mapping.
        /// </summary>
        private IMapper Mapper { get; }

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">Variable for data mapping.</param>
        public TeacherService(IUnitOfWork uow, IMapper mapper)
        {
            DataBase = uow;
            Mapper = mapper;
        }

        /// <summary>
        /// Gets all the teachers models.
        /// </summary>
        /// <returns>List of teachers models</returns>
        public IEnumerable<TeacherModel> GetAll()
        {
            return Mapper.Map<IEnumerable<Teacher>, List<TeacherModel>>(DataBase.TeacherRepository.FindAll().ToList());
        }

        /// <summary>
        /// Gets teacher model from Teacher Repository by id async.
        /// </summary>
        /// <param name="id">Teacher id</param>
        /// <returns>Teacher model</returns>
        public async Task<TeacherModel> GetByIdAsync(int id)
        {
            return Mapper.Map<TeacherModel>(await DataBase.TeacherRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Gets teacher model from Teacher Repository by user id async.
        /// </summary>
        /// <param name="id">Teacher id</param>
        /// <returns>Teacher model</returns>
        public async Task<TeacherModel> GetByUserId(int id)
        {
            return Mapper.Map<TeacherModel>(await DataBase.TeacherRepository.GetByAsync(t => t.UserId == id));
        }

        /// <summary>
        /// Adds teacher to database through Teacher Repository async.
        /// </summary>
        /// <param name="model">Teacher Model</param>
        public async Task AddAsync(TeacherModel model)
        {
            var teacher = Mapper.Map<Teacher>(model);

            await DataBase.TeacherRepository.AddAsync(teacher);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Updates teacher in database through Teacher Repository async.
        /// </summary>
        /// <param name="model">Teacher model</param>
        public async Task UpdateAsync(TeacherModel model)
        {
            var teacher = Mapper.Map<Teacher>(model);

            DataBase.TeacherRepository.Update(teacher);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Deletes teacher in database by modelId through Teacher Repository async.
        /// </summary>
        /// <param name="modelId">Teacher Model id</param>
        public async Task DeleteByIdAsync(int modelId)
        {
            await DataBase.TeacherRepository.DeleteByIdAsync(modelId);
            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Gets user model from database through Teacher Repository by async.
        /// </summary>
        /// <param name="model">Teacher Model</param>
        /// <returns>Teacher Model</returns>
        public async Task<TeacherModel> GetByAsync(TeacherModel model)
        {
            var teacher = await DataBase.TeacherRepository.GetByAsync(s => s.UserId == model.UserId);

            return Mapper.Map<TeacherModel>(teacher);
        }
    }
}