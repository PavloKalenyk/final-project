﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class GroupRepository : IGroupRepository
    {
        /// <summary>
        /// Object of TestingDbContext to access the database context.
        /// </summary>
        private readonly TestingDbContext _dataBase;

        /// <summary>
        /// Object instantiation.
        /// </summary>
        /// <param name="context">Object of TestingDbContext to access the database context.</param>
        public GroupRepository(TestingDbContext context)
        {
            _dataBase = context;
        }

        /// <summary>
        /// Gets all groups in database.
        /// </summary>
        /// <returns>All groups entities in database</returns>
        public IQueryable<Group> FindAll()
        {
            return _dataBase.Groups;
        }

        /// <summary>
        /// Gets group from database by id async.
        /// </summary>
        /// <param name="id">id of group</param>
        /// <returns>Group entity from database</returns>
        public async Task<Group> GetByIdAsync(int id)
        {
            return await _dataBase.Groups.FindAsync(id);
        }

        /// <summary>
        /// Adds group entity to database.
        /// </summary>
        /// <param name="entity">Group entity</param>
        public async Task AddAsync(Group entity)
        {
            await _dataBase.Groups.AddAsync(entity);
            await _dataBase.SaveChangesAsync();
        }

        /// <summary>
        /// Updates group entity in database.
        /// </summary>
        /// <param name="entity">Group entity</param>
        public void Update(Group entity)
        {
            _dataBase.Entry(entity).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes group entity.
        /// </summary>
        /// <param name="entity">Group entity</param>
        public void Delete(Group entity)
        {
            _dataBase.Groups.Remove(entity);
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes group entity in database by id async.
        /// </summary>
        /// <param name="id">id of group entity</param>
        public async Task DeleteByIdAsync(int id)
        {
            Group group = await _dataBase.Groups.FindAsync(id);

            if (group != null)
            {
                _dataBase.Groups.Remove(group);
                await _dataBase.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Gets group entity from database by expression async.
        /// </summary>
        /// <param name="expression">Expression by which to search for the entity of the group.</param>
        /// <returns>Group Entity</returns>
        public async Task<Group> GetByAsync(Expression<Func<Group, bool>> expression)
        {
            return await _dataBase.Groups.FirstOrDefaultAsync(expression);
        }
    }
}