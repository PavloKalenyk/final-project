import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QuestionAnswer } from '../interfaces/QuestionAnswer';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  rootUrl = 'https://localhost:44367/api/Tests';

  qns: any[];
  seconds: number;
  timer;
  qnProgress: number;
  correctAnswerCount: number = 0;
  testId: number;
  testName: string;
  currTest: number;
  studentId: number;
  teacherId: number;

  constructor(private http: HttpClient) { }

  displayTimeElapsed(): string {
    return this.getParcedTime(this.seconds);
  }

  getParcedTime(inputSeconds: number): string {
    if (inputSeconds >= 3600) {
      return Math.floor(inputSeconds / 3600) + 'h' + Math.floor(inputSeconds / 60) + 'm' + Math.floor(inputSeconds % 60) + 's';
    }
    else if (inputSeconds >= 60) {
      return Math.floor(inputSeconds / 60) + 'm' + Math.floor(inputSeconds % 60) + 's';
    }

    return Math.floor(inputSeconds % 60) + 's';
  }

  getQuestions(id: number) {
    console.log("questions: ");
    console.log(this.qns);
    return this.http.get(this.rootUrl + '/' + id);
  }

  getTests(id: number): Observable<any> {
    return this.http.get(this.rootUrl + "/tests?groupId=" + id + "&studentId=" + this.studentId);
  }

  getAnswers() {
    console.log("qns");
    console.log(this.qns);
    var body = this.qns.map(x => x.qnId);
    console.log(body);
    return this.http.post(this.rootUrl + `/answers?id=${this.testId}`, body);
  }

  submitResult() {
    let body = {
      studentId: this.studentId,
      testResult: this.correctAnswerCount,
      time: this.seconds,
      testId: this.testId,
      testName: this.testName,
      IsPassedTest: true
    };

    return this.http.post(this.rootUrl + `/result`, body);
  }

  getStudentResults(): Observable<any[]> {
    return this.http.get<any[]>(this.rootUrl + '/studentResults?studentId=' + this.studentId);
  }

  addTestName(groupName: string): any {
    var body = {
      name: this.testName,
      teacherId: this.teacherId
    };
    console.log("teacherId is");
    console.log(this.teacherId);

    return this.http.post(this.rootUrl + '/test?groupName=' + groupName, body);
  }

  addTest(questionAnswer: QuestionAnswer) {
    console.log("questionaire is");
    console.log(questionAnswer);
    let answerNum = questionAnswer["answer"];
    answerNum--;

    const body = {
      qn: questionAnswer["qn"],
      option1: questionAnswer["option1"],
      option2: questionAnswer["option2"],
      option3: questionAnswer["option3"],
      option4: questionAnswer["option4"],
      answer: answerNum
    };
    return this.http.post(this.rootUrl + '/questionAnswer?testName=' + this.testName, body);
  }

  getTeacherCurrentTests(): Observable<any[]> {
    return this.http.get<any[]>(this.rootUrl + '/teacherCurrentTests?' + 'testName=' + this.testName);
  }

  getResultsForTeacher(): Observable<any[]> {
    return this.http.get<any[]>(this.rootUrl + '/getResultsForTeacher?' + 'testId=' + this.testId);
  }

  getTeacherTests(): Observable<any[]> {
    return this.http.get<any[]>(this.rootUrl + '/getTeacherTests?' + 'teacherId=' + this.teacherId);
  }

  deleteTest(testId: number): Observable<any> {
    return this.http.delete(this.rootUrl + '/' + testId);
  }

  deleteQuestionAnswer(qaId: number): Observable<any> {
    return this.http.delete(this.rootUrl + '/deleteQa?qaId=' + qaId);
  }

  updateTest(test: any) {
    console.log("Update is work good!");

    return this.http.put(this.rootUrl, test);
  }

  updateQuestionAnswer(qa: QuestionAnswer) {
    console.log("work3");
    return this.http.put(this.rootUrl + '/putQuestionAnswer', qa);
  }
}
