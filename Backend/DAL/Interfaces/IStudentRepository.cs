﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IStudentRepository : IRepository<Student>
    {
        Task<Student> GetByAsync(Expression<Func<Student, bool>> expression);
    }
}