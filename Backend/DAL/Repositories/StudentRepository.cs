﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        /// <summary>
        /// Object of TestingDbContext to access the database context.
        /// </summary>
        private readonly TestingDbContext _dataBase;

        /// <summary>
        /// Object instantiation.
        /// </summary>
        /// <param name="context">Object of TestingDbContext to access the database context.</param>
        public StudentRepository(TestingDbContext context)
        {
            _dataBase = context;
        }

        /// <summary>
        /// Gets all students in database.
        /// </summary>
        /// <returns>All students entities in database</returns>
        public IQueryable<Student> FindAll()
        {
            return _dataBase.Students;
        }

        /// <summary>
        /// Gets student from database by id async.
        /// </summary>
        /// <param name="id">id of student</param>
        /// <returns>Student entity from database</returns>
        public async Task<Student> GetByIdAsync(int id)
        {
            return await _dataBase.Students.FindAsync(id);
        }

        /// <summary>
        /// Adds student entity to database.
        /// </summary>
        /// <param name="entity">Student entity</param>
        public async Task AddAsync(Student entity)
        {
            await _dataBase.Students.AddAsync(entity);
            await _dataBase.SaveChangesAsync();
        }

        /// <summary>
        /// Updates student entity in database.
        /// </summary>
        /// <param name="entity">Student entity</param>
        public void Update(Student entity)
        {
            _dataBase.Entry(entity).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes student entity.
        /// </summary>
        /// <param name="entity">Student entity</param>
        public void Delete(Student entity)
        {
            _dataBase.Students.Remove(entity);
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes student entity in database by id async.
        /// </summary>
        /// <param name="id">id of student entity</param>
        public async Task DeleteByIdAsync(int id)
        {
            Student student = await _dataBase.Students.FindAsync(id);

            if (student != null)
            {
                _dataBase.Students.Remove(student);
                await _dataBase.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Gets student entity from database by expression async.
        /// </summary>
        /// <param name="expression">Expression by which to search for the entity of the student.</param>
        /// <returns>Student Entity</returns>
        public async Task<Student> GetByAsync(Expression<Func<Student, bool>> expression)
        {
            return await _dataBase.Students.FirstOrDefaultAsync(expression);
        }
    }
}