﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class ResultRepository : IResultRepository
    {
        /// <summary>
        /// Object of TestingDbContext to access the database context.
        /// </summary>
        private readonly TestingDbContext _dataBase;

        /// <summary>
        /// Object instantiation.
        /// </summary>
        /// <param name="context">Object of TestingDbContext to access the database context.</param>
        public ResultRepository(TestingDbContext context)
        {
            _dataBase = context;
        }

        /// <summary>
        /// Gets all results in database.
        /// </summary>
        /// <returns>All result entities in database</returns>
        public IQueryable<Result> FindAll()
        {
            return _dataBase.Results;
        }

        /// <summary>
        /// Gets result from database by id async.
        /// </summary>
        /// <param name="id">id of result</param>
        /// <returns>Result entity from database</returns>
        public async Task<Result> GetByIdAsync(int id)
        {
            return await _dataBase.Results.FindAsync(id);
        }

        /// <summary>
        /// Adds Result entity to database.
        /// </summary>
        /// <param name="entity">Result entity</param>
        public async Task AddAsync(Result entity)
        {
            await _dataBase.Results.AddAsync(entity);
            await _dataBase.SaveChangesAsync();
        }

        /// <summary>
        /// Updates result entity in database.
        /// </summary>
        /// <param name="entity">Result entity</param>
        public void Update(Result entity)
        {
            _dataBase.Entry(entity).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes result entity.
        /// </summary>
        /// <param name="entity">Result entity</param>
        public void Delete(Result entity)
        {
            _dataBase.Results.Remove(entity);
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes result entity in database by id async.
        /// </summary>
        /// <param name="id">id of result entity</param>
        public async Task DeleteByIdAsync(int id)
        {
            Result result = await _dataBase.Results.FindAsync(id);

            if (result != null)
            {
                _dataBase.Results.Remove(result);
                await _dataBase.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Gets result entity from database by expression async.
        /// </summary>
        /// <param name="expression">Expression by which to search for the entity of the result.</param>
        /// <returns>Result Entity</returns>
        public async Task<Result> GetByAsync(Expression<Func<Result, bool>> expression)
        {
            return await _dataBase.Results.FirstOrDefaultAsync(expression);
        }
    }
}