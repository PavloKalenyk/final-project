﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Teacher
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}