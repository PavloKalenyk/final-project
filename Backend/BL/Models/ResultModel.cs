﻿using System.ComponentModel.DataAnnotations;
using DAL.Entities;

namespace BL.Models
{
    public class ResultModel
    {
        public int Id { get; set; } 
        public int StudentId { get; set; }
        public StudentModel Student { get; set; }
        public int TestResult { get; set; }
        public int Time { get; set; }
        public int? TestId { get; set; }
        public TestModel Test { get; set; }
        public bool IsPassedTest { get; set; }
    }
}