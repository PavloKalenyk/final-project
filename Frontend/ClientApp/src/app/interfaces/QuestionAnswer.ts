export class QuestionAnswer {
  qnId: number;
  qn: string;
  option1: string;
  option2: string;
  option3: string;
  option4: string;
  answer: number;
}
