﻿
namespace BL.Models
{
    public class StudentModel
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public GroupModel Group { get; set; }
        public int UserId { get; set; }
        public UserModel User { get; set; }
    }
}