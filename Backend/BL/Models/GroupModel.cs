﻿using System.Collections.Generic;

namespace BL.Models
{
    public class GroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TestModel> Tests { get; set; }
    }
}