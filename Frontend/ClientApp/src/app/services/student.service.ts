import { Student } from './../interfaces/Student';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import { Teacher } from '../interfaces/Teacher';
import { BaseUser } from '../interfaces/BaseUser';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  getUrl = 'https://localhost:44367/api/Users';

  getUrl2 = 'https://localhost:44367/api/Students';

  getUrl3 = 'https://localhost:44367/api/Teachers';

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.getUrl);
  }

  getStudents(): Observable<User[]> {
    return this.http.get<User[]>(this.getUrl + '/' + "students");
  }

  getStudent(id: number): Observable<Student> {
    console.log("id is");
    console.log(id);

    return this.http.get<Student>(this.getUrl2 + '/' + id);
  }

  getTeachers(): Observable<User[]> {
    return this.http.get<User[]>(this.getUrl + '/' + "teachers");
  }

  getTeacher(id: number): Observable<Teacher> {
    console.log("User id is");
    console.log(id);

    return this.http.get<Teacher>(this.getUrl3 + '/' + id);
  }

  addUser(user: User): Observable<any> {
    return this.http.post(this.getUrl, user);
  }

  deleteStudent(id: number): Observable<any> {
    return this.http.delete(this.getUrl + '/' + id);
  }

  updateUser(user: User): Observable<any> {
    return this.http.put(this.getUrl, user);
  }

  getBaseUsers(): Observable<BaseUser[]> {
    return this.http.get<BaseUser[]>(this.getUrl + '/' + "baseUsers");
  }

  addBaseUser(baseUser: BaseUser): Observable<any> {
    return this.http.post(this.getUrl, baseUser);
  }

  deleteBaseUser(id: number): Observable<any> {
    return this.http.delete(this.getUrl + '/' + id);
  }

  updateBaseUser(baseUser: BaseUser): Observable<any> {
    return this.http.put(this.getUrl, baseUser);
  }
}
