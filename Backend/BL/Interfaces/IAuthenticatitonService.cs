﻿using System.Threading.Tasks;
using BL.Models;

namespace BL.Interfaces
{
    public interface IAuthenticationService
    {
        Task RegisterAsync(UserRegisterModel userRegister);

        Task<UserLoggedModel> Authenticate(UserAuthModel userAuthModel);
    }
}