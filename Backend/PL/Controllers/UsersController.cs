﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Constants;
using BL.Interfaces;
using BL.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        /// <summary>
        /// Variable of IUserService type to access the User Service.
        /// </summary>
        private readonly IUserService  _userService;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="userService">Variable of IUserService type to access the User Service.</param>
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: api/<UsersController>
        [Authorize(Roles = Roles.Admin)]
        [HttpGet]
        public IEnumerable<UserModel> Get()
        {
            try
            {
                var users = _userService.GetAll();

                return users;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }   

        // GET: api/<UsersController>/students
        [Authorize(Roles = Roles.Admin)]
        [HttpGet("students")]
        public IEnumerable<UserModel> GetStudents()
        {
            try
            {
                var students = _userService.GetStudents();

                return students;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<UsersController>/teachers
        [Authorize(Roles = Roles.Admin)]
        [HttpGet("teachers")]
        public IEnumerable<UserModel> GetTeachers()
        {
            try
            {
                var teachers = _userService.GetTeachers();

                return teachers;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET: api/<UsersController>/baseUsers
        [Authorize(Roles = Roles.Admin)]
        [HttpGet("baseUsers")]
        public IEnumerable<UserModel> GetBaseUsers()
        {
            try
            {
                var baseUsers = _userService.GetBaseUsers();

                return baseUsers;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }


        // POST api/<StudentsController>
        [Authorize(Roles = Roles.Admin)]
        [HttpPost]
        public async Task<UserModel> AddUser([FromBody] UserModel userModel)
        {
            try
            {
                await _userService.AddAsync(userModel);

                var user = await _userService.GetByAsync(userModel);

                return user;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<StudentsController>/userModel
        [HttpPut]
        public async Task UpdateUser(UserModel userModel)
        {
            try
            {
                await _userService.UpdateAsync(userModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<StudentsController>/5
        [Authorize(Roles = Roles.Admin)]
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            try
            {
                await _userService.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
