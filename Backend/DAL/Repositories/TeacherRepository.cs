﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TeacherRepository : ITeacherRepository
    {
        /// <summary>
        /// Object of TestingDbContext to access the database context.
        /// </summary>
        private readonly TestingDbContext _dataBase;

        /// <summary>
        /// Object instantiation.
        /// </summary>
        /// <param name="context">Object of TestingDbContext to access the database context.</param>
        public TeacherRepository(TestingDbContext context)
        {
            _dataBase = context;
        }

        /// <summary>
        /// Gets all teachers in database.
        /// </summary>
        /// <returns>All teachers entities in database</returns>
        public IQueryable<Teacher> FindAll()
        {
            return _dataBase.Teachers;
        }

        /// <summary>
        /// Gets teacher from database by id async.
        /// </summary>
        /// <param name="id">id of teacher</param>
        /// <returns>Teacher entity from database</returns>
        public async Task<Teacher> GetByIdAsync(int id)
        {
            return await _dataBase.Teachers.FindAsync(id);
        }

        /// <summary>
        /// Adds teacher entity to database.
        /// </summary>
        /// <param name="entity">Teacher entity</param>
        public async Task AddAsync(Teacher entity)
        {
            await _dataBase.Teachers.AddAsync(entity);
            await _dataBase.SaveChangesAsync();
        }

        /// <summary>
        /// Updates teacher entity in database.
        /// </summary>
        /// <param name="entity">Teacher entity</param>
        public void Update(Teacher entity)
        {
            _dataBase.Entry(entity).State = EntityState.Modified;
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes teacher entity.
        /// </summary>
        /// <param name="entity">Teacher entity</param>
        public void Delete(Teacher entity)
        {
            _dataBase.Teachers.Remove(entity);
            _dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes teacher entity in database by id async.
        /// </summary>
        /// <param name="id">id of teacher entity</param>
        public async Task DeleteByIdAsync(int id)
        {
            Teacher teacher = await _dataBase.Teachers.FindAsync(id);

            if (teacher != null)
            {
                _dataBase.Teachers.Remove(teacher);
                await _dataBase.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Gets teacher entity from database by expression async.
        /// </summary>
        /// <param name="expression">Expression by which to search for the entity of the teacher.</param>
        /// <returns>Teacher Entity</returns>
        public async Task<Teacher> GetByAsync(Expression<Func<Teacher, bool>> expression)
        {
            return await _dataBase.Teachers.FirstOrDefaultAsync(expression);
        }
    }
}