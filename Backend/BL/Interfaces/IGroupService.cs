﻿using System.Threading.Tasks;
using BL.Models;

namespace BL.Interfaces
{
    public interface IGroupService : ICrud<GroupModel>
    {
        public Task<GroupModel> GetByAsync(GroupModel model);
    }
}