﻿namespace BL.Models
{
    public class TeacherModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public UserModel User { get; set; }
    }
}