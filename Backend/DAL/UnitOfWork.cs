﻿using System.Threading.Tasks;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TestingDbContext _dataBase;
        private UserRepository _userRepository;
        private GroupRepository _groupRepository;
        private StudentRepository _studentRepository;
        private TeacherRepository _teacherRepository;
        private QuestionAnswerRepository _questionAnswerRepository;
        private TestRepository _testRepository;
        private ResultRepository _resultRepository;

        public UnitOfWork()
        {
            _dataBase = new TestingDbContext(new DbContextOptions<TestingDbContext>());
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_dataBase);
                return _userRepository;
            }
        }

        public IGroupRepository GroupRepository
        {
            get
            {
                if (_groupRepository == null)
                    _groupRepository = new GroupRepository(_dataBase);
                return _groupRepository;
            }
        }

        public IStudentRepository StudentRepository
        {
            get
            {
                if (_studentRepository == null)
                    _studentRepository = new StudentRepository(_dataBase);
                return _studentRepository;
            }
        }

        public ITeacherRepository TeacherRepository
        {
            get
            {
                if (_teacherRepository == null)
                    _teacherRepository = new TeacherRepository(_dataBase);
                return _teacherRepository;
            }
        }

        public IQuestionAnswerRepository QuestionAnswerRepository
        {
            get
            {
                if(_questionAnswerRepository == null)
                    _questionAnswerRepository = new QuestionAnswerRepository(_dataBase);
                return _questionAnswerRepository;
            }
        }

        public ITestRepository TestRepository
        {
            get
            {
                if (_testRepository == null)
                    _testRepository = new TestRepository(_dataBase);
                return _testRepository;
            }
        }

        public IResultRepository ResultRepository
        {
            get
            {
                if (_resultRepository == null)
                    _resultRepository = new ResultRepository(_dataBase);
                return _resultRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _dataBase.SaveChangesAsync();
        }
    }
}