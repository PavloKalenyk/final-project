import { User } from './user';

export interface Student {
  id: number;
  groupId: number;
  userId: number;
  user: User;
}
