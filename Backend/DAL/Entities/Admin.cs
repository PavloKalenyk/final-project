﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Admin
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        private User User { get; set; }
    }
}