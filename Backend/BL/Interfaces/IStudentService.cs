﻿using System.Threading.Tasks;
using BL.Models;

namespace BL.Interfaces
{
    public interface IStudentService : ICrud<StudentModel>
    {
        public Task<StudentModel> GetByUserId(int id);
        public Task<StudentModel> GetByAsync(StudentModel model);

    }
}