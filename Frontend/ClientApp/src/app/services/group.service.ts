import { Group } from './../interfaces/Group';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  getUrl = 'https://localhost:44367/api/Groups';

  constructor(private http: HttpClient) { }

  getGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.getUrl);
  }

  getGroup(id: number): Observable<Group> {
    return this.http.get<Group>(this.getUrl + '/' + id);
  }

  addGroup(group: Group): Observable<any> {
    return this.http.post(this.getUrl, group);
  }

  deleteGroup(id: number): Observable<any> {
    return this.http.delete(this.getUrl + '/' + id);
  }

  updateGroup(group: Group): Observable<any> {
    return this.http.put(this.getUrl, group);
  }
}
