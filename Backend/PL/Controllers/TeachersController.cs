﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Interfaces;
using BL.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeachersController : ControllerBase
    {
        /// <summary>
        /// Variable of ITeacherService type to access the Teacher Service.
        /// </summary>
        private readonly ITeacherService _teacherService;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="teacherService">Variable of TeacherService type to access the Teacher Service.</param>
        public TeachersController(ITeacherService teacherService)
        {
            _teacherService = teacherService;
        }

        // GET: api/<TeachersController>
        [HttpGet]
        public IEnumerable<TeacherModel> Get()
        {
            try
            {
                return _teacherService.GetAll();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET api/<TeachersController>/5
        [HttpGet("{userId}")]
        public async Task<TeacherModel> GetByUserId(int userId)
        {
            try
            {
                return await _teacherService.GetByUserId(userId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<TeachersController>
        [HttpPost]
        public async Task<TeacherModel> AddTeacher([FromBody] TeacherModel teacherModel)
        {
            try
            {
                await _teacherService.AddAsync(teacherModel);

                return await _teacherService.GetByAsync(teacherModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<TeachersController>/5
        [HttpPut("{id}")]
        public async Task UpdateTeacher(TeacherModel teacherModel)
        {
            try
            {
                await _teacherService.UpdateAsync(teacherModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<TeachersController>/5
        [HttpDelete("{id}")]
        public async Task DeleteTeacher(int id)
        {
            try
            {
                await _teacherService.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
