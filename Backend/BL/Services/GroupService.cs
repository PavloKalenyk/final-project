﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.Interfaces;
using BL.Models;
using BL.Validation;
using DAL.Entities;
using DAL.Interfaces;

namespace BL.Services
{
    /// <summary>
    /// Group Service for group actions.
    /// </summary>
    public class GroupService : IGroupService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private IUnitOfWork DataBase { get; }

        /// <summary>
        /// Variable of IMapper type for data mapping.
        /// </summary>
        private IMapper Mapper { get; }

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">Variable for data mapping.</param>
        public GroupService(IUnitOfWork uow, IMapper mapper)
        {
            DataBase = uow;
            Mapper = mapper;
        }

        /// <summary>
        /// Gets all the groups models.
        /// </summary>
        /// <returns>List of groups models</returns>
        public IEnumerable<GroupModel> GetAll()
        {
            return Mapper.Map<IEnumerable<Group>, List<GroupModel>>(DataBase.GroupRepository.FindAll().ToList());
        }

        /// <summary>
        /// Gets group model from Group Repository by id async.
        /// </summary>
        /// <param name="id">Group id</param>
        /// <returns>Group model</returns>
        public async Task<GroupModel> GetByIdAsync(int id)
        {
            return Mapper.Map<GroupModel>(await DataBase.GroupRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Adds group to database through Group Repository async.
        /// </summary>
        /// <param name="model">Group Model</param>
        public async Task AddAsync(GroupModel model)
        {
            if(string.IsNullOrEmpty(model.Name))
                throw new TestingException("The group name field is empty!", "");

            var groupModel = await DataBase.GroupRepository.GetByAsync(g => g.Name == model.Name);

            if (groupModel != null)
            {
                throw new TestingException($"This group: \"{groupModel.Name}\" already exist!", "");
            }

            var group = Mapper.Map<Group>(model);

            await DataBase.GroupRepository.AddAsync(group);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Updates group in database through Group Repository async.
        /// </summary>
        /// <param name="model">Group model</param>
        public async Task UpdateAsync(GroupModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                throw new TestingException("The group name field is empty!", "");
            }

            var groupModel = await DataBase.GroupRepository.GetByAsync(g => g.Name == model.Name);

            if (groupModel != null)
            {
                throw new TestingException($"This group name: \"{groupModel.Name}\" already exist!", "");
            }

            var group = Mapper.Map<Group>(model);

            DataBase.GroupRepository.Update(group);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Deletes group in database by modelId through Group Repository async.
        /// </summary>
        /// <param name="modelId">Group Model id</param>
        public async Task DeleteByIdAsync(int modelId)
        {
            var group = await DataBase.GroupRepository.GetByIdAsync(modelId);

            if (group == null)
            {
                throw new TestingException("The group does not exist!", "");
            }

            var student = DataBase.StudentRepository.FindAll().FirstOrDefault(s => s.GroupId == modelId);
            var test = DataBase.TestRepository.FindAll().FirstOrDefault(s => s.GroupId == modelId);

            if (student != null || test != null)
            {
                throw new TestingException("This group cannot be deleted because it has users or tests.", "");
            }

            await DataBase.GroupRepository.DeleteByIdAsync(modelId);
            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Gets group model from database through Group Repository by async.
        /// </summary>
        /// <param name="model">Group Model</param>
        /// <returns>Group Model</returns>
        public async Task<GroupModel> GetByAsync(GroupModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                throw new TestingException("The group name field is empty!", "");
            }

            var group = await DataBase.GroupRepository.GetByAsync(g => g.Name == model.Name);
            
            return Mapper.Map<GroupModel>(group);
        }
    }
}