﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Interfaces;
using BL.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        /// <summary>
        /// Variable of IAuthenticationService type to access the Authentication Service.
        /// </summary>
        private readonly IAuthenticationService _authenticationService;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="authenticationService">Variable of IAuthenticationService type to access the Authentication Service.</param>
        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        // POST api/<AuthenticationController>/register
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task Register([FromBody] UserRegisterModel userRegister)
        {
            try
            {
                await _authenticationService.RegisterAsync(userRegister);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<AuthenticationController>    
        [AllowAnonymous]
        [HttpPost]
        public async Task<UserLoggedModel> Login(UserAuthModel userAuthModel)
        {
            try
            {
                return await _authenticationService.Authenticate(userAuthModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
