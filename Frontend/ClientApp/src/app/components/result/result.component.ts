import { Component, OnInit } from '@angular/core';
import { TestService } from 'src/app/services/test.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  constructor(public testService: TestService, private router: Router) { }

  ngOnInit(): void {

    // if (parseInt(localStorage.getItem('qnProgress')) != 10) {
    //   console.log("yessss");

    //   this.router.navigate(['/student-information']);
    // }

    if (parseInt(localStorage.getItem('qnProgress')) == 10) {
      this.testService.seconds = parseInt(localStorage.getItem('seconds'));
      this.testService.qnProgress = parseInt(localStorage.getItem('qnProgress'));
      this.testService.qns = JSON.parse(localStorage.getItem('qns'));
      this.testService.testId = parseInt(localStorage.getItem('testId'));
      this.testService.studentId = parseInt(localStorage.getItem('studentId'));
      this.testService.getAnswers().subscribe(
        (data: any) => {
          this.testService.correctAnswerCount = 0;
          this.testService.qns.forEach((e, i) => {
            if (e.answer == data[i])
              this.testService.correctAnswerCount++;
            e.correct = data[i];
          });
        }
      );
    }
    else
      this.router.navigate(['/test']);
  }

  OnSubmit() {
    this.testService.submitResult().subscribe((data) => {
      localStorage.setItem('qnProgress', "0");
      localStorage.setItem('qns', "");
      localStorage.setItem('seconds', "0");
      localStorage.setItem('testId', "0");
      localStorage.setItem('studentId', "0");
      localStorage.setItem('currTest', "0");
      this.router.navigate(['/studentInformation']);
    });
  }
}
