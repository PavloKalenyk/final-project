﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository { get; }

        IGroupRepository GroupRepository { get; }

        IStudentRepository StudentRepository { get; }

        ITeacherRepository TeacherRepository { get; }

        IQuestionAnswerRepository QuestionAnswerRepository { get; }
        IResultRepository ResultRepository { get; }
        ITestRepository TestRepository { get; }

        Task<int> SaveAsync();
    }
}