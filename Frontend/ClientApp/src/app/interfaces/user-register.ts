export interface UserRegister {
  userId: number;
  login: string;
  firstName: string;
  lastName: string;
  groupName: string;
  role: string;
  password: string;
}
