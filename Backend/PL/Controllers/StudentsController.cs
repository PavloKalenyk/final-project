﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Interfaces;
using BL.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        /// <summary>
        /// Variable of IStudentService type to access the Student Service.
        /// </summary>
        private readonly IStudentService _studentService;

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="studentService">Variable of IStudentService type to access the Student Service.</param>
        public StudentsController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        // GET: api/<StudentController>
        [HttpGet]
        public IEnumerable<StudentModel> Get()
        {
            try
            {
                return _studentService.GetAll();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET api/<StudentController>/5
        [HttpGet("{userId}")]
        public async Task<StudentModel> GetByUserId(int userId)
        {
            try
            {
                return await _studentService.GetByUserId(userId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<StudentController>
        [HttpPost]
        public async Task<StudentModel> AddStudent([FromBody] StudentModel studentModel)
        {
            try
            {
                await _studentService.AddAsync(studentModel);

                return await _studentService.GetByAsync(studentModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<StudentController>/5
        [HttpPut("{id}")]
        public async Task UpdateStudent(StudentModel studentModel)
        {
            try
            {
                await _studentService.UpdateAsync(studentModel);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<StudentController>/5
        [HttpDelete("{id}")]
        public async Task DeleteStudent(int id)
        {
            try
            {
                await _studentService.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
