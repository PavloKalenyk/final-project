﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.Interfaces;
using BL.Models;
using DAL.Entities;
using DAL.Interfaces;

namespace BL.Services
{
    /// <summary>
    /// Student Service for group actions.
    /// </summary>
    public class StudentService : IStudentService
    {
        /// <summary>
        /// Variable of IUnitOfWork type to access the database.
        /// </summary>
        private IUnitOfWork DataBase { get; }

        /// <summary>
        /// Variable of IMapper type for data mapping.
        /// </summary>
        private IMapper Mapper { get; }

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="uow">Variable for database access.</param>
        /// <param name="mapper">Variable for data mapping.</param>
        public StudentService(IUnitOfWork uow, IMapper mapper)
        {
            DataBase = uow;
            Mapper = mapper;
        }

        /// <summary>
        /// Gets all the students models.
        /// </summary>
        /// <returns>List of students models</returns>
        public IEnumerable<StudentModel> GetAll()
        {
            return Mapper.Map<IEnumerable<Student>, List<StudentModel>>(DataBase.StudentRepository.FindAll().ToList());
        }

        /// <summary>
        /// Gets student model from Student Repository by id async.
        /// </summary>
        /// <param name="id">Group id</param>
        /// <returns>Student model</returns>
        public async Task<StudentModel> GetByIdAsync(int id)
        {
            var studentModel = Mapper.Map<StudentModel>(await DataBase.StudentRepository.GetByIdAsync(id));
            return studentModel;
        }

        /// <summary>
        /// Gets student model from Student Repository by user id async.
        /// </summary>
        /// <param name="id">Group id</param>
        /// <returns>Student model</returns>
        public async Task<StudentModel> GetByUserId(int id)
        {
            var studentModel = Mapper.Map<StudentModel>(await DataBase.StudentRepository.GetByAsync(s => s.UserId == id));
            return studentModel;
        }

        /// <summary>
        /// Adds student to database through Student Repository async.
        /// </summary>
        /// <param name="model">Student Model</param>
        public async Task AddAsync(StudentModel model)
        {
            var student = Mapper.Map<Student>(model);

            await DataBase.StudentRepository.AddAsync(student);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Updates student in database through Student Repository async.
        /// </summary>
        /// <param name="model">Student model</param>
        public async Task UpdateAsync(StudentModel model)
        {
            var student = Mapper.Map<Student>(model);

            DataBase.StudentRepository.Update(student);

            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Deletes student in database by modelId through Student Repository async.
        /// </summary>
        /// <param name="modelId">Student Model id</param>
        public async Task DeleteByIdAsync(int modelId)
        {
            await DataBase.StudentRepository.DeleteByIdAsync(modelId);
            await DataBase.SaveAsync();
        }

        /// <summary>
        /// Gets user model from database through Student Repository by async.
        /// </summary>
        /// <param name="model">Student Model</param>
        /// <returns>Student Model</returns>
        public async Task<StudentModel> GetByAsync(StudentModel model)
        {
            var student = await DataBase.StudentRepository.GetByAsync(s=>s.UserId == model.UserId);

            return Mapper.Map<StudentModel>(student);
        }
    }
}