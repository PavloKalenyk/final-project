﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IResultRepository : IRepository<Result>
    {
        Task<Result> GetByAsync(Expression<Func<Result, bool>> expression);
    }
}