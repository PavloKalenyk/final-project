import { Student } from './Student';
import { Teacher } from './Teacher';

export class BaseUser {
  id: number;
  firstName: string;
  lastName: string;
  student: any;
  teacher: any;
  login: string;
  role: string;
  token: string;
  isActivate: boolean;
  password: string;
  groupName: string;
}
