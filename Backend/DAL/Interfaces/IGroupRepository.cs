﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IGroupRepository : IRepository<Group>
    {
        Task<Group> GetByAsync(Expression<Func<Group, bool>> expression);
    }
}